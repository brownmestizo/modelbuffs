$(document).ready(function() {

    $('#shippingStands').on("change", function(e) {
        var standPrice = parseFloat($(this).find(":selected").data("price"));

        //pass to a hidden variable
        $('#_standPrice').val(standPrice);

        //compute prices
        $('#finalPrice').text(computeTotalPrice());

        $('#standName').text($(this).find(":selected").text());
        $('#standTotalPrice').text(computeTotalStandPrice());
        $('#standUnitPrice').text(displayCurrency(standPrice));

        $('#modelPrice').css('display', 'table');
        $('#addonsPrices').css('display', 'table');
    });

});

