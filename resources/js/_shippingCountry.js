$(document).ready(function() {

    $('#shippingCountry').select2({
      placeholder: 'Select a country',
      allowclear: true,
      width: 385,
    });

    $('#shippingCountry').on("change", function(e) {
    	var priority_price = parseFloat($(this).find(":selected").data("priority"));
    	var economy_price = parseFloat($(this).find(":selected").data("economy"));
        var airparcel_price = parseFloat($(this).find(":selected").data("airparcel"));

    	$('.shipping-cost').css('display', 'block');

    	if (priority_price) {
    		$('#priorityPriceContainer').css('display', 'block');
    		$('#priorityPrice').text('Fedex Priority - $' + priority_price.toFixed(2));
            $('#priorityPriceVal').val(priority_price.toFixed(2));
    	} else {
    		$('#priorityPriceContainer').css('display', 'none');
    	}

    	if (economy_price) {
    		$('#economyPriceContainer').css('display', 'block');
    		$('#economyPrice').text('Fedex Economy - $' + economy_price.toFixed(2));
            $('#economyPriceVal').val(economy_price.toFixed(2));
    	} else {
    		$('#economyPriceContainer').css('display', 'none');
    	}

        if (airparcel_price) {
            var price = (airparcel_price*1.07)/44.5;
            $('#airparcelPriceContainer').css('display', 'block');
            $('#airparcelPrice').text('Air Parcel - $' + price.toFixed(2));
            $('#airparcelPriceVal').val(price.toFixed(2));
        } else {
            $('#airparcelPriceContainer').css('display', 'none');
        }

        $('.shipping-price-radio').prop('checked', false);
        $('#addonsShipping').css('display', 'none');
        $('#_shippingPrice').val(0);
        $('#finalPrice').text(computeTotalPrice());

    });


    $('#economyPriceVal').on("change", function(e) {
        $('#_shippingPrice').val($(this).val());
        $('#shippingName').text('Fedex Economy');
        $('#_shippingMethod').val('economy');

        $('#addonsShipping').css('display', 'block');
        $('#finalPrice').text(computeTotalPrice());
    });

    $('#priorityPriceVal').on("change", function(e) {
        $('#_shippingPrice').val($(this).val());
        $('#shippingName').text('Fedex Priority');
        $('#_shippingMethod').val('priority');

        $('#addonsShipping').css('display', 'block');
        $('#finalPrice').text(computeTotalPrice());
    });

    $('#airparcelPriceVal').on("change", function(e) {
        $('#_shippingPrice').val($(this).val());
        $('#shippingName').text('Air Parcel');
        $('#_shippingMethod').val('airparcel');

        $('#addonsShipping').css('display', 'block');
        $('#finalPrice').text(computeTotalPrice());
    });
});

