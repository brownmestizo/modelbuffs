function computeTotalPrice () {

    var unitPrice = parseFloat($('#_unitPrice').val());
    var quantity = parseFloat($('#qty').val());
    var stand = parseFloat($('#_standPrice').val()) * quantity;
    var shipping = parseFloat($('#_shippingPrice').val());

    var finalValue = (unitPrice * quantity) + stand + shipping;

    return displayCurrency(finalValue);
}

/*
function computeTotalShippingPrice () {
    var quantity = parseFloat($('#qty').val());
    var shipping = parseFloat($('#_shippingPrice').val());

    var finalValue = (shipping * quantity);

    return displayCurrency(finalValue);
}
*/

function computeTotalStandPrice () {
    var quantity = parseFloat($('#qty').val());
    var finalValue = parseFloat($('#_standPrice').val()) * quantity;

    return displayCurrency(finalValue);
}

function computeTotalModelPrice () {
    var quantity = parseFloat($('#qty').val());
    var model = parseFloat($('#_unitPrice').val());

    var finalValue = (model * quantity);

    return displayCurrency(finalValue);
}

function updateQty () {
    var quantity = parseFloat($('#qty').val());

    $('#standQty').text(quantity + 'x');
    $('#modelQty').text(quantity + 'x');
}

function displayCurrency (price) {
    return '$' + parseFloat(price).toFixed(2);
}