<?php
/* @var $this CategoryController */

	$this->breadcrumbs=array(
		'Category',
	);
?>

<h1><?php echo $category->menu_name; ?></h1>
<?php echo $category->encodedMenuWriteup(); ?>

<?php

	echo '<div>';
	$this->widget('CLinkPager', array('pages' => $pages,));
	echo '</div> <hr />';

	foreach ($products as $prod) {
?>

		<div class="small-12 medium-12 large-12 columns listRow">

			<div class="small-12 medium-4 large-3 columns">

				<a href="<?php echo Yii::app()->baseUrl.'/index.php?id='.$prod->prod_id.'&name='.$prod->prod_name; ?>">
					<?php echo Yii::app()->easyImage->thumbOf(Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_1, array('resize' => array('width' => 350, 'height' => 250), 'sharpen' => 50, 'background' => '#ffffff', 'type' => 'jpg','quality' => 60,)); ?>
				</a>
			</div>

			<div class="small-12 medium-8 large-9 columns">

				<a href="<?php echo Yii::app()->baseUrl.'/index.php?id='.$prod->prod_id.'&name='.$prod->prod_name; ?>">
					<h3><?php echo $prod->prod_name; ?></h3>
				</a>

					<?php
						if ($prod->displayPrice("$")) {
							echo '<span class="metaPriceContainer">';
							echo '<span class="metaPrice">'.$prod->displayPrice("$").'</span> ';
							echo ' '.$prod->getAttributeLabel('displayPrice');
							echo '</span>';
						}

						if ($prod->displaySmallerPrice("$")) {
							echo '<span class="metaPriceContainer metaSmallerPrice">';
							echo '<span class="metaPrice">'.$prod->displaySmallerPrice("$").'</span>';
							echo ' '.$prod->getAttributeLabel('displaySmallerPrice');
							echo '</span>';
						}
					?>


				<div class="clear"></div>

				<?php

					echo $prod->displayAttribute('prod_code', $prod->prod_code);
					echo $prod->displayAttribute('prod_length', $prod->displayInchesToCm($prod->prod_length, 'in'));
					echo $prod->displayAttribute('prod_wingspan', $prod->displayInchesToCm($prod->prod_wingspan, 'in'));
					echo $prod->displayAttribute('prod_height', $prod->displayInchesToCm($prod->prod_height, 'in'));
					echo $prod->displayAttribute('prod_scale', $prod->prod_scale);
					echo $prod->displayAttribute('prod_aircraftreg', $prod->prod_aircraftreg);

				?>

			</div>

		</div>

		<hr />
<?php

	}

	echo '<div>';
	$this->widget('CLinkPager', array('pages' => $pages,));
	echo '</div>';


?>


