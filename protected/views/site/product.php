<?php
$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/resources/js/select2/select2.js');
$cs->registerScriptFile($baseUrl.'/resources/js/colorbox/jquery.colorbox-min.js');
$cs->registerCSSFile($baseUrl.'/resources/js/select2/select2.css');
$cs->registerCSSFile($baseUrl.'/resources/js/colorbox/colorbox.css');

$cs->registerScriptFile($baseUrl.'/resources/js/_shippingCountry.js');
$cs->registerScriptFile($baseUrl.'/resources/js/_qtyspinner.js');

$cs->registerScript(
  "standsDropdown",
  "$(document).ready(function() {

	function format(stand) {
		var option = stand.element;
	    return \"<img style='width:35px; height:30px;' src='".Yii::app()->baseUrl."/resources/images/stands/\" + stand.id + \".jpg' /> <span>\"
	    + stand.text + ' - $' + parseFloat($(option).data('price')).toFixed(2)  + \"</span>\";
	}

    $('#shippingStands').select2({
      placeholder: 'Select a different stand',
      allowclear: true,
      width: 380,
      minimumResultsForSearch: -1,
      formatResult: format,
      formatSelection: format,
    });

  });",
  CClientScript::POS_END
);

$cs->registerScriptFile($baseUrl.'/resources/js/_stands.js');

$cs->registerScript(
  "colorbox",
  "$(document).ready(function() {

	$(\".group2\").colorbox({rel:'group2', transition:\"fade\"});

  });",
  CClientScript::POS_END
);
?>

<script>
$(document).ready(function(){

	function updateShipping(prodID){
	  var qty = $('#qty').val();
	  var country = $('#shippingCountry').find(":selected").val();
	  var zone = $('#shippingCountry').find(":selected").data("zone");
	  var shippingMethod = $('#_shippingMethod').val();
	  var weightIDE = $('#_weightIDE').val();
	  var weightIDP = $('#_weightIDP').val();
	  var finalzone;

	  if (zone) finalzone = zone; else finalzone="";

	  $.ajax({
	      url: "<?php echo Yii::app()->createUrl('site/addtlShipping'); ?>",
	      data: 'prodID='+prodID+'&qty='+qty+'&country='+country+'&zone='+finalzone+'&shippingMethod='+shippingMethod+'&weightIDE='+weightIDE+'&weightIDP='+weightIDP,
	      type: 'POST',
	      success: function(data) {
	        if (data) {
				$('#_shippingPrice').val(data);
				$('#shippingTotalPrice').text('$'+data);
				$('#standTotalPrice').text(computeTotalStandPrice());
				$('#modelTotalPrice').text(computeTotalModelPrice());
				$('#finalPrice').text(computeTotalPrice());	          
	        }
	      }
	    });
	}

    $('#qty').spinner({ min: 1, max:9 });
	$('#qty').spinner().change(function(){
        updateShipping($('#_prodID').val());
        updateQty();
		$('#standTotalPrice').text(computeTotalStandPrice());
		$('#modelTotalPrice').text(computeTotalModelPrice());
		$('#finalPrice').text(computeTotalPrice());	                  
	});

	$('.ui-spinner-button').click(function() { $(this).siblings('input').change(); });

    $('#economyPriceVal').on("change", function(e) {
        updateShipping($('#_prodID').val());
	});

    $('#priorityPriceVal').on("change", function(e) {
        updateShipping($('#_prodID').val());
	});

    $('#airparcelPriceVal').on("change", function(e) {
        updateShipping($('#_prodID').val());
	});

});
</script>
<div class="product-page">

	<div class="clear"></div>
	<div class="row">

		<!-- Photos -->
		<div class="medium-12 large-4 columns">
			<a href="<?php echo Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_1; ?>" class="group2">
				<?php echo Yii::app()->easyImage->thumbOf(Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_1,
				array('resize' => array('width' => 600, 'height' => 350), 'sharpen' => 50, 'background' => '#ffffff', 'type' => 'jpg','quality' => 70,)); ?>
			</a>

			<div class="clear"></div>

			<a href="<?php echo Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_2; ?>" class="group2">
			<?php echo Yii::app()->easyImage->thumbOf(Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_2,
			array('resize' => array('width' => 260, 'height' => 100), 'sharpen' => 50, 'background' => '#ffffff', 'type' => 'jpg','quality' => 60,)); ?>
			</a>

			<a href="<?php echo Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_3; ?>" class="group2">
			<?php echo Yii::app()->easyImage->thumbOf(Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_3,
			array('resize' => array('width' => 260, 'height' => 100), 'sharpen' => 50, 'background' => '#ffffff', 'type' => 'jpg','quality' => 60,)); ?>
			</a>

			<a href="<?php echo Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_4; ?>" class="group2">
			<?php echo Yii::app()->easyImage->thumbOf(Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_4,
			array('resize' => array('width' => 260, 'height' => 100), 'sharpen' => 50, 'background' => '#ffffff', 'type' => 'jpg','quality' => 60,)); ?>
			</a>

			<script>
			    jQuery('a.gallery').colorbox();
			</script>

			<div class="panel callout radius" style="margin-top: 10px;">
				<span class="small-print">
				For other destinations, please email sales@modelbuffs.com and we will send
	    		 you a tailor made quotation for the delivery to your country including directions
	    		  on how to make a payment direct to Paypal.
	    		</span>
			</div>
		</div>
		<!-- End of Photos -->

		<!-- Other Info -->
		<div class="medium-12 large-8 columns">

			<div class="large-12 columns" style="border-bottom: 1px solid #EFEFEF; margin-bottom: 5px;">
				<div style="display: block; float: left;"><h1><?php echo $prod->prod_name; ?></h1></div>
				<div class="align-right"><h1 class="grey-text"><?php echo $prod->displayPrice("$"); ?></h1></div>

				<input type="hidden" value="<?php echo $prod->prod_id; ?>" id="_prodID">
				<input type="hidden" value="<?php echo $prod->displayPrice(""); ?>" id="_unitPrice">
				<input type="hidden" value="0" id="_shippingPrice">
				<input type="hidden" value="0" id="_standPrice">
				<input type="hidden" value="" id="_shippingMethod">
				<input type="hidden" value="<?php echo $prod->shippingCategory->weight_ide; ?>" id="_weightIDE">
				<input type="hidden" value="<?php echo $prod->shippingCategory->weight_idp; ?>" id="_weightIDP">
			</div>

			<!-- Start of Column 2 -->
			<div class="large-6 columns">
				<?php
					if ($prod->prod_code) {
						echo '<div style="display:table; width:100%; margin-bottom: 10px;" class="prodcode">';
			        	echo '<div class="col" style="width:25%; display: table; float: left; padding: 5px;"><strong>'.$prod->getAttributeLabel('prod_code').'</strong></div>';
			        	echo '<div class="col" style="width:75%; display: table;  padding: 5px;">'.$prod->prod_code.'</div>';
			        	echo '</div>';
					}
				?>

	        	<div class="meta clearfix">
					<?php
						if ($prod->prod_length) {
				        	echo '<div class="col"><strong>'.$prod->getAttributeLabel('prod_length').'</strong></div>';
				        	echo '<div class="col">'.$prod->displayInchesToCm($prod->prod_length, 'in').'</div>';
						}

						if ($prod->prod_wingspan) {
				        	echo '<div class="col"><strong>'.$prod->getAttributeLabel('prod_wingspan').'</strong></div>';
				        	echo '<div class="col">'.$prod->displayInchesToCm($prod->prod_wingspan, 'in').'</div>';
						}

						if ($prod->prod_height) {
				        	echo '<div class="col"><strong>'.$prod->getAttributeLabel('prod_height').'</strong></div>';
				        	echo '<div class="col">'.$prod->displayInchesToCm($prod->prod_height, 'in').'</div>';
						}

						if ($prod->prod_scale) {
				        	echo '<div class="col"><strong>'.$prod->getAttributeLabel('prod_scale').'</strong></div>';
				        	echo '<div class="col">'.$prod->prod_scale.'</div>';
						}

						if ($prod->prod_aircraftreg) {
				        	echo '<div class="col"><strong>'.$prod->getAttributeLabel('prod_aircraftreg').'</strong></div>';
				        	echo '<div class="col">'.$prod->prod_aircraftreg.'</div>';
						}
					?>
				</div>

				<?php
					if ($prod->displaySmallerPrice("$")) {
						echo '<div class="data-block">';
						echo 'There is a smaller version of this model for '.$prod->displaySmallerPrice("$");
						echo '</div>';
					}
				?>

				<div class="clearfix">
					<h3>Choose your stand</h3>
					<span class="small-print">A desk stand (solid mahogany or solid mahogany with stainless steel arm) is
					included in the price of the model. </span>

					<select id="shippingStands" style="margin-top: 10px;">
					<option></option>
			        <?php
			        	foreach($stands as $stand) {
			        		echo '<option value="'.$stand->stand_id.'" data-price="'.$stand->stand_price.'">';
			        		echo $stand->stand_name;
			        		echo '</option>';
			        	}
			        ?>
					</select>
				</div>

			</div>
			<!-- End of Column 2 -->

			<!-- Start of Column 3 -->
			<div class="large-6 columns">


				<form method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr" target="paypal">
					<input type="hidden" name="cmd" value="_cart">
					<input type="hidden" name="add" value="1">
					<input type="hidden" name="business" value="adrian@rathmell.org">
					<input type="hidden" name="item_name" value="<?php echo $prod->prod_name; ?>">
					<input type="hidden" name="item_number" value="<?php echo $prod->prod_code; ?>">
					<input type="hidden" name="amount" value="<?php echo $prod->displayPrice(""); ?>">
					<input type="hidden" name="shipping" value="1.00">
					<input type="hidden" name="shipping2" value="0.50">
					<input type="hidden" name="handling" value="2.00">
					<input type="hidden" name="currency_code" value="USD">
		            <input type="hidden" value="1" name="quantity">
					<input type="hidden" name="return" value="<?php echo Yii::app()->request->baseUrl; ?>">
					<input type="hidden" name="cancel_return" value="<?php echo Yii::app()->request->baseUrl; ?>">
					<!-- system-generated hiden variables -->
					<input type="hidden" value="<?php echo $prod->displayPrice(""); ?>" name="final_price">
					<!-- -->
					<!-- <input type="submit" name="submit" value="Add to cart" class="download button expand"> -->
				</form>

				<div class="clearfix" style="margin-bottom: 10px;">
					<h3>Estimate Shipping Price </h3>
					<span class="small-print">To estimate the shipping price, please select the country of
						destination. <em><strong>Note:</strong> Production approximately 8-10 weeks from date of order confirmation.</em></span>

					<select id="shippingCountry" style="margin-top: 10px;">
					<option></option>
			        <?php
			        	foreach($countries as $country) {
			        		echo '<option value="'.$country->cty_id.'" data-zone="'.$country->shipping_zone->zone_name.'"';
			        		echo ' data-airparcel="'.$country->airparcel_firstkg.'"';
			        		echo ' data-economy="'.$country->computeEconomyPrice($prod->shippingCategory->weight_ide).'"';
			        		echo ' data-priority="'.$country->computePriorityPrice($prod->shippingCategory->weight_idp).'">';
			        		echo ucwords(strtolower($country->cty_name));
			        		echo '</option>';
			        	}
			        ?>
					</select>

					<span class="shipping-cost" style="display: none;">
						<h3 style="margin-bottom: 5px;">Choose a shipping speed</h3>
						<div id="economyPriceContainer">
						<input type="radio" name="shipping-price-container" class="shipping-price-radio" value="" id="economyPriceVal">
							<label for="economyPriceVal" id="economyPrice">Economy via Fedex</label>
							<div class="clearfix"></div>
						</div>

						<div id="priorityPriceContainer">
						<input type="radio" name="shipping-price-container" class="shipping-price-radio" value="" id="priorityPriceVal">
							<label for="priorityPriceVal" id="priorityPrice">Priority via Fedex</label>
						</div>

						<div id="airparcelPriceContainer">
						<input type="radio" name="shipping-price-container" class="shipping-price-radio" value="" id="airparcelPriceVal">
							<label for="airparcelPriceVal" id="airparcelPrice">Air Parcel</label>
						</div>
					</span>
				</div>
			</div>
			<!-- End of Column 3 -->

			<div class="large-12 columns" style="margin-top: 10px;">
		    	<div class="data-block section-prices">

					<div class="large-4 columns">
						<img src="http://placehold.it/250x150">
					</div>
					<div class="large-8 columns">
						<div id="regularPrice" class="price-line-item">
							<div class="first-col" id="modelName"><?php echo $prod->prod_name; ?></div>
							<div class="second-col" id="modelUnitPrice"><?php echo $prod->displayPrice("$"); ?></div>
							<div class="third-col" id="modelQty">1x</div>
							<div class="fourth-col" id="modelTotalPrice"><?php echo $prod->displayPrice("$"); ?></div>
						</div>
						<div id="addonsPrices" class="price-line-item" style="display: none;">
							<div class="first-col" id="standName">[Stand Name]</div>
							<div class="second-col" id="standUnitPrice">Unit Price</div>
							<div class="third-col" id="standQty">1x</div>
							<div class="fourth-col" id="standTotalPrice">[Price]</div>
						</div>
						<div id="addonsShipping" class="price-line-item" style="display: none;">
							<div class="first-col" id="shippingName">[Shipping Speed]</div>
							<div class="second-col" id="shippingUnitPrice"></div>
							<div class="third-col" id="shippingQty"></div>
							<div class="fourth-col" id="shippingTotalPrice">[Price]</div>
						</div>


						<div class="clearfix "></div>

						<span class="left"><h3><strong>Total Price</strong></h3></span>
						<span class="right" id="finalPrice"><?php echo $prod->displayPrice("$"); ?></span>


						<div class="large-12 columns clearfix" style="height: 10px;"></div>

						<div class="large-2 columns">
							<input type="text" value="1" id="qty" name="qty">
						</div>
						<div class="large-6 columns">
							<span class="full-width-btn">
								<?php echo CHtml::link('Add to Cart', CController::createUrl('/checkout?id='.$prod->prod_id), array('id' => 'checkout', 'class'=>'full-width-btn button button-green right')); ?>
							</span>
						</div>
					</div>




	        	</div>
			</div>
		</div>
		<!-- End of Other Info -->
	</div>

	<hr />

	<!-- Start of Product Description -->
	<div class="row">
		<div class="large-12 columns">
			<dl class="tabs" data-tab>
				<dd class="active"><a href="#panel1">Description</a></dd>
				<dd><a href="#panel2">Postage and Payments</a></dd>
			</dl>

			<div class="tabs-content">
				<div class="content active" id="panel1">
					<?php echo $prod->displayContent('prod_writeup', $prod->prod_writeup);  ?>
					<?php echo $prod->displayContent('generalDescription', $prod->generalDescription->displayDescription($prod->getAlternateNames()));  ?>
				</div>

				<div class="content" id="panel2">
					<?php echo $prod->displayContent('prod_description', $prod->prod_description);  ?>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Product Description -->

</div>


