<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

$cs->registerScriptFile($baseUrl.'/resources/js/select2/select2.js');
$cs->registerCSSFile($baseUrl.'/resources/js/select2/select2.css');
$cs->registerScriptFile($baseUrl.'/resources/js/_shippingCountry.js');
$cs->registerScriptFile($baseUrl.'/resources/js/_qtyspinner.js');

$cs->registerScript(
  "countryDropdown",
  "$(document).ready(function() {

	$('#checkoutButton').click(function(){
		$('#shippingSection').hide();
		$('#checkoutSection').show('slow',function(){});
	});

  });",
  CClientScript::POS_END
);

?>

<div class="checkout-page">

	<div class="clear"></div>

	<div class="row">
		<h1>Shopping Cart</h1>


		<input type="hidden" value="<?php echo $prod->displayPrice(""); ?>" id="_unitPrice">

		<div class="data-block-nobg cart-items" id="cartItems">

			<table>
				<tr>
					<td class="first-col header">Item</td>
					<td class="second-col header">Price</td>
					<td class="third-col header">Qty</td>
					<td class="fourth-col header"></td>
					<td class="fifth-col header">Total</td>
				</tr>
				<tr>
					<td class="first-col">
					<?php
					echo Yii::app()->easyImage->thumbOf(Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_1,
					array('resize' => array('width' => 50, 'height' => 50), 'sharpen' => 50, 'background' => '#ffffff', 'type' => 'jpg','quality' => 70,)); ?>
					<?php echo $prod->prod_name; ?>
					</td>
					<td class="second-col"><?php echo $prod->displayPrice("$"); ?></td>
					<td class="third-col"><input type="text" value="1" id="qty" name="qty"></td>
					<td class="fourth-col"><input type="button" class="button" value="Remove" /></td>
					<td class="fifth-col"><span id="finalPrice"><?php echo $prod->displayPrice("$"); ?></span></td>
				</tr>
			</table>

		</div>
	</div>

	<div class="row" id="shippingSection">
		<div class="medium-12 large-6 columns">
			<div class="data-block clearfix">
				Content here
			</div>
		</div>

		<div class="medium-12 large-6 columns">
			<div class="data-block clearfix">
				<h3>Shipping Options</h3>
				<p>To estimate the shipping price, please select the country of destination.</p>

				<select id="shippingCountry" style="margin-bottom: 10px;">
				<option></option>
		        <?php
		        	foreach($countries as $country) {
		        		echo '<option value="'.$country->cty_id.'" data-zone="'.$country->shipping_zone->zone_name.'"';
		        		echo ' data-economy="'.$country->computeEconomyPrice($prod->shippingCategory->weight_ide).'"';
		        		echo ' data-priority="'.$country->computePriorityPrice($prod->shippingCategory->weight_idp).'">';
		        		echo ucwords(strtolower($country->cty_name));
		        		echo '</option>';
		        	}
		        ?>
				</select>



				<span class="shipping-cost" style="display: none;">

					<h3 style="margin-bottom: 5px;">Choose a shipping speed</h3>

					<div id="economyPriceContainer" style="display: none;">
					<input type="radio" name="shipping-price-container" value="" id="economyPriceVal">
						<label for="economyPriceVal" id="economyPrice">Economy via Fedex</label>
						<div class="clearfix"></div>
					</div>

					<div id="priorityPriceContainer" style="display: none;">
					<input type="radio" name="shipping-price-container" value="" id="priorityPriceVal">
						<label for="priorityPriceVal" id="priorityPrice">Priority via Fedex</label>
					</div>	

				</span>

				<div class="clearfix">
				<a href="#" class="button button-green right" id="checkoutButton">Checkout</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="checkoutSection" style="display: none;">

		<div class="medium-12 large-6 columns">
			<div class="data-block clearfix">
				<form>
					<h3>Checkout</h3>
					<div class="row">
					<div class="large-12 columns">
					  <label>E-mail address
					    <input type="text" placeholder="E-mail address" />
					  </label>
					</div>
					</div>

					<div class="row">
					<div class="large-12 columns">
					  <label>Title</label>
					  <input type="radio" name="pokemon" value="Red" id="pokemonRed"><label for="pokemonRed">Mr</label>
					  <input type="radio" name="pokemon" value="Blue" id="pokemonBlue"><label for="pokemonBlue">Mrs</label>
					  <input type="radio" name="pokemon" value="Blue" id="pokemonBlue"><label for="pokemonBlue">Ms</label>
					</div>
					</div>

					<div class="row">
					<div class="large-12 columns">
					  <label>First Name
					    <input type="text" placeholder="First Name" />
					  </label>
					</div>
					</div>

					<div class="row">
					<div class="large-12 columns">
					  <label>Last Name
					    <input type="text" placeholder="Last Name" />
					  </label>
					</div>
					</div>

					<div class="row">
					<div class="large-12 columns">
					  <label>Company
					    <input type="text" placeholder="Company" />
					  </label>
					</div>
					</div>

					<div class="row">
					<div class="large-12 columns">
					  <label>Phone
					    <input type="text" placeholder="Phone" />
					  </label>
					</div>
					</div>
			</div>
		</div>

		<div class="medium-12 large-6 columns">
			<div class="data-block clearfix">




				  <div class="row">
				    <div class="large-12 columns">
				      <label>Address
				        <input type="text" placeholder="Address" />
				      </label>
				    </div>
				  </div>

				  <div class="row">
				    <div class="large-12 columns">
				      <label>City
				        <input type="text" placeholder="City" />
				      </label>
				    </div>
				  </div>

				  <div class="row">
				    <div class="large-12 columns">
				      <label>State
				        <input type="text" placeholder="State" />
				      </label>
				    </div>
				  </div>

				  <div class="row">
				    <div class="large-12 columns">
				      <label>Zip / Postal Code
				        <input type="text" placeholder="Zip / Postal Code" />
				      </label>
				    </div>
				  </div>

				  <div class="row">
				    <div class="large-12 columns">
				      <label>Country

						<select id="e2">
						<option></option>
				        <?php
				        	foreach($countries as $country) {
				        		echo '<option value="'.$country->cty_id.'" data-zone="'.$country->shipping_zone->zone_name.'">';
				        		echo ucwords(strtolower($country->cty_name))."-".$country->shipping_zone->zone_id;
				        		echo '</option>';
				        	}
				        ?>
						</select>
				      </label>
				    </div>
				  </div>



				  <div class="row">
					<div class="large-12 columns">
				      <p>Data Privacy</p>
					  <p>The personal data you provide is used to answer queries, process orders or allow access to specific information. You have the right to modify and delete all the personal information found in the "My Account" page.</p>
				    </div>
				  </div>


				</form>

				<div class="clearfix">
				<a href="#" class="button button-green right">Confirm Payment</a>
				</div>
			</div>
		</div>
	</div>

	<hr />


</div>


