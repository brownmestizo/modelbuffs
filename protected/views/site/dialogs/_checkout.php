<?php
Yii::app()->clientscript->scriptMap['jquery-ui.min.js'] = false;

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
	'id'=>'dialog',
	'options'=>array(
	    'title'=>'Order Fulfill',
	    'autoOpen'=>true,
	    'modal'=>'true',
      	'width'=>'574',
	    'height'=>'auto',
      	'resizable'=>false,
      	'open'=>"js:function() {
        $('.ui-widget-overlay').bind('click',function(){
          $('#dialog').dialog('close');
        });
      }",
	),
));

?>

Hello Dialog

<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>