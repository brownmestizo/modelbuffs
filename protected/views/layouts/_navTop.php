	<div class="">

		<nav data-topbar="" class="top-bar docs-bar hide-for-small row">
		  <ul class="title-area">
		    <li class="name">
		      <h1><?php echo CHtml::link('Modelbuffs', Yii::app()->request->baseUrl); ?></h1>
		    </li>
		  </ul>

		  <section class="top-bar-section">
		    <ul class="right">
		      <li class="has-dropdown not-click">
		        <a class="" href="#">Categories</a>
		        <ul class="dropdown">
                <?php $this->widget('application.components.NavMenuWidget', array('menuCategory'=>'categories',)); ?>
		        </ul>
		      </li>
			  <?php $this->widget('application.components.NavMenuWidget', array('menuCategory'=>'navTop',)); ?>
		      <li class="has-form">
		        <a class="small button" href="contact.php">Contact Us</a>
		      </li>
		    </ul>
		  </section>
		</nav>

	</div>