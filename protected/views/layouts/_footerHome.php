			<div id="newsletter">
				<div class="row">
					<div class="medium-8 columns">
					  <h5>Stay on top of what's happening with Modelbuffs..</h5>
					  <p>Enter your e-mail address to join our mailing list and get updated with our offers and new products.</p>
					</div>


					<div class="medium-4 columns">
						<form method="post" action="#">
							<div class="row collapse margintop-20px">
					    		<div class="small-8 medium-8 columns">
					      			<input type="text" name="email" placeholder="signup@example.com">
					    		</div>
					    		<div class="small-4 medium-4 columns">
					      			<input type="submit" href="#" class="postfix small button expand" value="Sign Up">
					    		</div>
					  		</div>
						</form>
					</div>
				</div>
			</div>

			<div class="zurb-footer-top bg-fblue">
				<div class="row property">
					<div class="medium-4 columns">
			  			<div class="property-info">
						    <h3>Modelbuffs</h3>
						    <p>ModelBuffs is a one stop factory source for handmade museum quality
						     wood model airplanes and wooden helicopter models</p>
						     <p>Most models are made to order and take about 10-12 weeks* to manufacture 
						     	(even customised or personalised models) which is significantly
						     	 quicker than most of our competitors. </p>
						</div>
					</div>

					<div class="medium-8 columns">
						<div class="row collapse">

						    <div class="medium-4 columns">
						      <div class="learn-links">
						        <h4 class="hide-for-small">The Modelbuffs Advantage</h4>
						        <ul>
						          <li>Quality</li>
						          <li>Workmanship</li>
						          <li>Cost</li>
						          <li>Customer Service</li>
						        </ul>
						      </div>
						  	</div>

						    <div class="medium-4 columns">
						      <div class="support-links">
						        <h4 class="hide-for-small">Talk to us</h4>
						        <p>Tweet us at <br> <a href="https://twitter.com/modelbuffs">@modelbuffs</a></p>
						        <p>Email us at <br> <a href="mailto:info@modelbuffs.com">info@modelbuffs.com</a><br> or check our <a href="#">support page</a></p>
						      </div>
						    </div>

						  	<div class="medium-4 columns">
						      <div class="connect-links">
						        <h4 class="hide-for-small">Stay in touch</h4>
						        <p>Keep up with the latest on our products. Read more about us on <a href="#">latest products</a>.</p>
						        <a href="#" class="small button">Latest Products</a>
						      </div>
						    </div>

						</div>
					</div>

				</div>

                <?php # $this->renderPartial('/layouts/_footerHomeSub'); ?>

			</div>

			<div class="footer-bottom">
				<div class="row">
					<div class="large-12 columns">
						<p class="copyright">Trademark and Copyright Notice: Paypal, Yahoo Messenger and MSN Messenger and other related entities. All rights reserved. Paypal, Yahoo Messenger and 
						MSN Messenger graphics and other materials available here are protected by the copyright and trademark laws of their respective companies and is not 
						related or affiliated in anyway to the aforementioned companies.</p>
					</div>
				</div>


				<div class="row">
					<div class="medium-4 medium-4 push-8 columns">
					  <ul class="home-social">
					      <li><a href="http://www.twitter.com/modelbuffs" class="twitter"></a></li>
					      <li><a href="http://www.facebook.com/modelbuffs" class="facebook"></a></li>
					      <li><a href="#" class="mail"></a></li>
					    </ul>
					 </div>

					 <div class="medium-8 medium-8 pull-4 columns">
					    <ul class="zurb-links">
                			<?php
                				$this->widget('application.components.NavMenuWidget', array('menuCategory'=>'navBottom',)); 
                			?>
					 	</ul>
					 	<p class="copyright">P.O. #5240 Angeles City P.O. 2009 Pampanga, Philippines</p>
					   	<p class="copyright">&copy; 2008&dash;2013 Modelbuffs.com. All Rights Reserved.</p>
					</div>
				</div>
			</div>