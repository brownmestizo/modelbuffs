  <div class="hide-for-small">
      <div class="sidebar">
		  <img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/images/logo-mb.png" />
		  <form style="margin-top: 10px;">
			<?php
				  $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
				      'attribute'=>'my_name',
				        'model'=>$model,
				        'sourceUrl'=>array('site/aclist'),
				        'name'=>'my_input_name',
				        'options'=>array(
				          'minLength'=>'1',
				        ),
				        'htmlOptions'=>array(
					        'id'=>'automcomplete',
					        'type'=>'search',
					        'placeholder'=>'Search Products: e.g. Learjet, Boeing',
				        ),
				  )); 
			  ?>
		  </form>

		  <nav>
		    <ul class="side-nav">
				<?php $this->widget('application.components.NavMenuWidget', array('menuCategory'=>'categories')); ?>
		    </ul>
		  </nav>

		  <a href="#" class="download button expand">Made to Order Models</a>

		  <section id="sidebarFeaturedProducts">
		    <ul>
		    	<!-- loop this one -->
				<li>
				<a href="#" target="_blank">
				  <span class="productName">Pitts S-2S</span>
				  <span class="productCategory"><span>Military Airplanes Propeller</span></span>
				</a>
				</li>
		    </ul>
		    <a id="via" href="#">via <span class="sidebarFeaturedProducts-link">Latest Products</span></a>
		  </section>

	  </div>
  </div>
