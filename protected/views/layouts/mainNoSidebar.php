<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en">
<![endif]-->

<!--[if IE 7 ]>
<html class="ie ie7" lang="en">
<![endif]-->

<!--[if IE 8 ]>
<html class="ie ie8" lang="en">
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
  <head>

    <!-- Meta Tags
    ================================================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php
    $cs=Yii::app()->getClientScript();

    // Register Default Meta Tags
    Yii::app()->clientScript->registerMetaTag("ModelBuffs is your one stop factory source for handmade museum quality wood model airplanes and wooden helicopter models.  Stock, bespoke and custom made personalised scale handcrafted and hand painted wooden model aircraft, helicopters, spacecraft, rockets, airships and blimps, tail fins, boats, submarines  and ships, military wall plaques and seals all made out of solid kiln dried, renewable Philippine mahogany", "description", null, null, "description");
    Yii::app()->clientScript->registerMetaTag("Modelbuffs, Model Buffs, Factory, handmade museum quality, wood model airplanes, wooden helicopter models custom made scale handcrafted, hand painted wooden model aircraft, Made to Order, spacecraft, rockets, airships, airship, blimps, tail fins, boats, submarines, submarine, ships, ship, boat, military wall plaques , seals, solid kiln dried Philippine mahogany, British authentic aircraft, Private, Civilian, Military Jet, Propeller, Glider, Science Fiction, Space NASA Experimental, nose art aviation, collectibles, executive Gifts, promotional incentives, hand carved, Pacific, handpainted, logo, carved, stand, authentic, personalized, direct, helicopters, replica, bespoke, blueprints, Clark Air Base, Warplane, Warplanes, bravo delta", "keywords", null, null, "keywords");
    Yii::app()->clientScript->registerMetaTag("Modelbuffs Philippines", "author", null, null, "author");
    ?>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- CSS
    ================================================== -->
    <link href='<?php echo Yii::app()->getRequest()->isSecureConnection? 'https':'http'; ?>://fonts.googleapis.com/css?family=Dosis:600,500,300' rel='stylesheet' type='text/css'>
    <link href='<?php echo Yii::app()->getRequest()->isSecureConnection? 'https':'http'; ?>://fonts.googleapis.com/css?family=Open+Sans:400,600,400italic' rel='stylesheet' type='text/css'>
    <link href='<?php echo Yii::app()->getRequest()->isSecureConnection? 'https':'http'; ?>://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/foundation.css">
    <!--<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/docs.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/custom.css">


    <!-- JS
    ==================================================
    -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/resources/js/modernizr.js"></script>

    <!--[if IE 7 ]>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css_fonts/font-awesome-ie7.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css_browsers/ie7.css" type="text/css" />
    <![endif]-->

    <!--[if IE 6]>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css_browsers/ie6.css" type="text/css" />
    <![endif]-->

    <!--[if lt IE 9]>
    <script async src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico">
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-57x57.png">
    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-72x72.png">
    <!-- For iPhone with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-114x114.png">
    <!-- For third-generation iPad with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-144x144.png">


  </head>

  <body class="antialiased hide-extras">
  <!-- Primary Page Layout
  ================================================== -->


      <div class="marketing off-canvas-wrap">
        <div class="inner-wrap">

          <title>Modelbuffs Philippines</title>
          <meta name="description" content="Modelbuffs Philippines" />

          <!-- <nav class="tab-bar show-for-small">
            <a class="off-canvas-left-toggle menu-icon ">
              <span></span>
            </a>
          </nav> -->

          <?php $this->renderPartial('/layouts/_navTop'); ?>

          <nav class="tab-bar show-for-small">
            <a class="left-off-canvas-toggle menu-icon">
              <span>Modelbuffs</span>
            </a>
          </nav>

          <?php $this->renderPartial('/layouts/_sidebarMobile'); ?>

            <section role="main">
                <div class="row">
                   <form style="margin-top: 10px;">
                    <?php
                      $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                          'attribute'=>'my_name',
                            'model'=>$model,
                            'sourceUrl'=>array('site/aclist'),
                            'name'=>'my_input_name',
                            'options'=>array(
                              'minLength'=>'1',
                            ),
                            'htmlOptions'=>array(
                              'id'=>'automcomplete',
                              'type'=>'search',
                              'placeholder'=>'Search Products: e.g. Learjet, Boeing',
                            ),
                      )); 
                    ?>
                  </form>                      
                </div>
                <div class="row mainContent">
                  <div class="large-12 medium-12 columns">
                      <!-- Content -->
                      <?php echo $content; ?>
                      <!-- End Content -->
                  </div>
                </div>
                <?php $this->renderPartial('/layouts/_footerHome'); ?>
            </section>

        </div>
      </div>


    <!-- Google Analytics
    ================================================== -->
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-334321-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/resources/js/vendor/fastclick.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/resources/js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>