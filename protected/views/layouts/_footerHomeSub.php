				<div class="row global">
					<div class="medium-3 small-6 columns">
					  <a href="http://zurb.com/studios" class="footer-link-block services">
					    <span class="title">Studios</span>
					    <span>Helping more than 200 startups succeed since 1998.</span>
					  </a>
					</div>
					<div class="medium-3 small-6 columns">
					  <a href="http://foundation.zurb.com/" class="footer-link-block foundation">
					    <span class="title">Foundation</span>
					    <span>The most advanced front-end framework in the world.</span>
					  </a>
					</div>
					<div class="medium-3 small-6 columns">
					  <a href="http://zurb.com/apps" class="footer-link-block apps">
					    <span class="title">Design Apps</span>
					    <span>Prototype, iterate and collect feedback on your products.</span>
					  </a>
					</div>
					<div class="medium-3 small-6 columns">
					  <a href="http://zurb.com/university" class="footer-link-block expo">
					    <span class="title">University</span>
					    <span>Ideas, thoughts and design resources shared with you.</span>
					  </a>
					</div>
				</div>
