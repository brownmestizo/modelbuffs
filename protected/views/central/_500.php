<div class="alert-noresults">

  <h2><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/central/img/500.png" alt="500. That's an error."/></h2>

  <h2>Sorry, seems like something's gone wrong!</h2>
  <p>Thanks for noticing — we'll fix it up and have it things back to normal soon.</p>
  <p>If the problem persists, please <a href="//www.sdc.qld.edu.au/about/contact" target="_blank">let us know</a>.</p>

</div>

<div class="alert-warning">
  <p><strong>Error <?php echo $code; ?></strong> - <?php echo CHtml::encode($message); ?></p>
</div>