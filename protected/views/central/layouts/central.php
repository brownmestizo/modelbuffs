<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en">
<![endif]-->

<!--[if IE 7 ]>
<html class="ie ie7" lang="en">
<![endif]-->

<!--[if IE 8 ]>
<html class="ie ie8" lang="en">
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
  <head>

    <!-- Meta Tags
    ================================================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php
    $cs=Yii::app()->getClientScript();

    // Register Default Meta Tags
    Yii::app()->clientScript->registerMetaTag("ModelBuffs is your one stop factory source for handmade museum quality wood model airplanes and wooden helicopter models.  Stock, bespoke and custom made personalised scale handcrafted and hand painted wooden model aircraft, helicopters, spacecraft, rockets, airships and blimps, tail fins, boats, submarines  and ships, military wall plaques and seals all made out of solid kiln dried, renewable Philippine mahogany", "description", null, null, "description");
    Yii::app()->clientScript->registerMetaTag("Modelbuffs, Model Buffs, Factory, handmade museum quality, wood model airplanes, wooden helicopter models custom made scale handcrafted, hand painted wooden model aircraft, Made to Order, spacecraft, rockets, airships, airship, blimps, tail fins, boats, submarines, submarine, ships, ship, boat, military wall plaques , seals, solid kiln dried Philippine mahogany, British authentic aircraft, Private, Civilian, Military Jet, Propeller, Glider, Science Fiction, Space NASA Experimental, nose art aviation, collectibles, executive Gifts, promotional incentives, hand carved, Pacific, handpainted, logo, carved, stand, authentic, personalized, direct, helicopters, replica, bespoke, blueprints, Clark Air Base, Warplane, Warplanes, bravo delta", "keywords", null, null, "keywords");
    Yii::app()->clientScript->registerMetaTag("Modelbuffs Philippines", "author", null, null, "author");
    ?>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/central/central.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/central/navigation.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/central/typography.css">    
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/central/bookings.css">        
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/central/forms.css">            
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/central/buttons.css">            

    <link href='<?php echo Yii::app()->getRequest()->isSecureConnection? 'https':'http'; ?>://fonts.googleapis.com/css?family=Dosis:600,500,300' rel='stylesheet' type='text/css'>
    <link href='<?php echo Yii::app()->getRequest()->isSecureConnection? 'https':'http'; ?>://fonts.googleapis.com/css?family=Open+Sans:400,600,400italic' rel='stylesheet' type='text/css'>
    <link href='<?php echo Yii::app()->getRequest()->isSecureConnection? 'https':'http'; ?>://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700' rel='stylesheet' type='text/css'>    
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/font-awesome.min.css">

    

    <!-- JS
    ================================================== 
    -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/resources/js/modernizr.js"></script>

    <!--[if IE 7 ]>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css_fonts/font-awesome-ie7.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css_browsers/ie7.css" type="text/css" />
    <![endif]-->

    <!--[if IE 6]>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css_browsers/ie6.css" type="text/css" />
    <![endif]-->

    <!--[if lt IE 9]>
    <script async src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico">
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-57x57.png">
    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-72x72.png">
    <!-- For iPhone with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-114x114.png">
    <!-- For third-generation iPad with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon-144x144.png">


  </head>

  <body class="antialiased hide-extras clearfix">



  <div class="body-wrap clearfix">

    <div class="page-wrap clearfix">

      <!-- HEADER -->
      <header>
        <div class="logo">
          <?php echo CHtml::link('MPM<span class="meta-normal">Central</span>', array('/central')); ?>
        </div>

        <div id="dd" class="user">
            Adrian Rathmell <i class="icon-caret-down"></i>'
            <ul class="dropdown">
              <li><a href="<?php echo $this->createUrl('/myaccount'); ?>">My Account</a></li>
              <li><a href="<?php echo $this->createUrl('/logout'); ?>">Log out</a></li>
            </ul>
        </div>
        <div class="link" title="Central Home">

          <?php echo CHtml::link('<i class="icon-home"><!--[if lt IE 7 ]>Home<![endif]--></i>', array('../')); ?>

        </div>
      </header>

        <!-- NAVIGATION -->
        <nav role="navigation">
          <ul class="first-nav">
            <li>
              <?php echo CHtml::link('Dashboard', array('#'), array('class'=>'dashboard')); ?>
            </li>
            <li>
              <?php echo CHtml::link('Orders', array('#'), array('class'=>'accounts')); ?>
            </li>
            <li>
              <?php echo CHtml::link('Products', array('/central/addproduct'), array('class'=>'bookings')); ?>
            </li>
            <li>
              <?php echo CHtml::link('Settings', array('#'), array('class'=>'courses')); ?>
            </li>
            <li>
              <?php echo CHtml::link('Resources', array('#'), array('class'=>'resources')); ?>
            </li>
          </ul>

          <ul class="second-nav">
            <li>
              <?php echo CHtml::link('<span class="notification" id="taskPageCount">12</span> Tasks', array('/central/subnav/tasks'), array('class'=>'')); ?>
            </li>
          </ul>

          <div class="version">
          </div>

        </nav>

      <!-- CONTENT -->
      <div class="content-wrap">
        <?php echo $content; ?>
      </div>

    </div>

    </div>

     <!-- FOOTER -->
    <footer></footer>

    <!-- SCRIPTS -->
    <script type="text/javascript">

      $(document).ready(function() {
        // PNG fix for IE6
        $(document).pngFix();
      });

    </script>






  <script src="<?php echo Yii::app()->request->baseUrl; ?>/resources/js/jquery.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/resources/js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>
  
  </body>
</html>