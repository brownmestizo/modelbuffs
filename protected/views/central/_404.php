<div class="alert-noresults">

  <h2><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/central/img/404.png" alt="404. That's an error."/></h2>

	<h2>Sorry, we couldn’t find that page!</h2>
	<p>Thanks for noticing — we'll fix it up and have it things back to normal soon.</p>
	<p>If the problem persists, please <a href="//www.sdc.qld.edu.au/about/contact" target="_blank">let us know</a>.</p>

</div>

<div class="alert-warning">
	<p><strong>Error <?php echo $code; ?></strong> - <?php echo CHtml::encode($message); ?></p>
</div>