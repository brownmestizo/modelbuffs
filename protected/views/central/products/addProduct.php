<div id="new-booking-tab" class="clearfix">

        <div class="column-wrap clearfix">

            <div class="two-thirds-column">

              <div class="alert-error" id="new-booking_es_">
                <p><strong>Whoa there</strong>, please check and correct the following:</p>
                <ul>
                <li>Error 1</li>
                <li>Error 2</li>
                </ul>
              </div>

              <div class="padding-nomargin">

                <div id="details" class="data-container">
                  <h2>Add Product</h2>

                  <div class="data-area">

                    <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                    <p>
                      <label>Name</label>
                      <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                    </p>

                    <p>
                      <label>Code</label>
                      <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                    </p>

                    <p>
                      <label>Alternative Names</label>
                      <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                    </p>

                    <p>
                      <label>Related Link</label>
                      <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                    </p>                    

                    <p>
                      <label>Related Link Description</label>
                      <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                    </p>                    
                </div>

              </div>

              <div class="padding">

                  <div id="seoDetails" class="data-container">
                    <h2>SEO Details</h2>

                    <div class="data-area">
                      <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                      <p>
                        <label>Name</label>
                        <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                      </p>               
                    </div>
                  </div>

                  <div id="productDescription" class="data-container">
                    <h2>Product Description</h2>

                    <div class="data-area">
                      <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                      <p>
                        <label>Name</label>
                        <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                      </p>               
                    </div>
                  </div>

                  <div id="otherDetails" class="data-container">
                    <h2>Other Details</h2>

                    <div class="data-area">
                      <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                      <p>
                        <label>Name</label>
                        <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                      </p>               
                    </div>
                  </div>

                  <div id="pricing" class="data-container">
                    <h2>Pricing</h2>

                    <div class="data-area">
                      <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                      <p>
                        <label>Name</label>
                        <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                      </p>               
                    </div>
                  </div>

                  <div id="relatedProducts" class="data-container">
                    <h2>Related Products</h2>

                    <div class="data-area">
                      <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                      <p>
                        <label>Name</label>
                        <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                      </p>               
                    </div>
                  </div>                  


                  <div id="relatedProducts" class="data-container">
                    <h2>Images</h2>

                    <div class="data-area">
                      <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                      <p>
                        <label>Images</label>
                        <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                      </p>               
                    </div>
                  </div>                                    

                  <div id="notes" class="data-container">
                    <h2>Notes</h2>

                    <div class="data-area">
                      <div class="alert-error-form" id="Booking_bookingName_em_" style="display:none"></div>                    
                      <p>
                        <label>Notes</label>
                        <span class="full-width"><input name="Booking[bookingName]" type="text" maxlength="100"></span>
                      </p>               
                    </div>
                  </div>                        

            </div>

            <div class="one-third-column">

              <div id="sticky-nav" class="padding-nomargin" style="position: absolute; top: 131.59375px; left: 819px; margin: 0px; width: 265px;">
                  <ul class="navnav">
                    <li><a href="#details" class="stickynav_details"><i class="icon-chevron-left"></i> Main Details</a></li>
                    <li><a href="#contact" class="stickynav_contact"><i class="icon-chevron-left"></i> SEO Details</a></li>
                    <li><a href="#quote" class="stickynav_quote"><i class="icon-chevron-left"></i> Product Description</a></li>
                    <li><a href="#catering" class="stickynav_catering"><i class="icon-chevron-left"></i> Other Details</a></li>
                    <li><a href="#rooms" class="stickynav_rooms"><i class="icon-chevron-left"></i> Pricing</a></li>
                    <li><a href="#faculty" class="stickynav_faculty"><i class="icon-chevron-left"></i> Related Products</a></li>
                    <li><a href="#attachments" class="stickynav_attachments"><i class="icon-chevron-left"></i> Images</a></li>
                    <li><a href="#comments" class="stickynav_comments"><i class="icon-chevron-left"></i> Notes</a></li>
                  </ul>

              <div class="half-width-btn">
                <a id="cancel-new-booking" class="button-default" href="/central/bookings">
                <!--[if IE 6]><div class='ie-cancel-icon-accounts'></div><![endif]-->
                <i class="icon-remove"></i> Cancel</a>
                <a id="save-new-booking" class="button-green">
                <!--[if IE 6]><div class='ie-save-icon-accounts'></div><![endif]-->
                <i class="icon-ok"></i> Save</a>              '
              </div>

              </div>

            </div>

          

        </div>

      </div>