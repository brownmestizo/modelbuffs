<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

	<h1 id="side-nav"></h1>
	<p>ModelBuffs is a one stop factory source for handmade museum quality wood model airplanes and wooden helicopter models. Stock,
	 bespoke and custom made personalised scale handcrafted and hand painted wooden model aircraft, helicopters, spacecraft, rockets, airships and blimps, 
	 tail fins, boats, submarines  and ships, military wall plaques and seals all made out of solid kiln dried, renewable Philippine mahogany 
	 (commonly known as Lauan or Meranti).</p>

	<ul class="example-orbit" data-orbit data-options="animation:'fade';timer_speed:1000;navigation_arrows:false;slide_number:false;">
		<?php for($i=1; $i<=19; $i++) { ?>
	  		<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/images/homepageSlideshow/<?php echo $i; ?>.jpg" alt="Slide" /></li>
	  	<?php } ?>
	</ul>

	<hr>
	
	<h3>Who are we?</h3>

	<p>As a British owned company with manufacturing facilities in the Philippines, ModelBuffs.com® offers custom made
	and standard "off the shelf" authentic quality wooden model aircraft in Private & Civilian, Military / Warplane Jet, 
	Military Propeller Warplanes, Glider, Airship, Submarine, Ship, Boat, Science Fiction, Space, NASA and Experimental
	categories - even  aviation "nose art" panels.  Unlike our competitors ModelBuffs.com® run their business direct
	from the factory source and not through third party companies or regional trading offices.</p>

		<div class="clear">

			<?php foreach ($featuredItems as $key=>$prod) { ?>
				<div class="small-6 medium-3 large-3 columns metaProd">
					<a href="<?php echo Yii::app()->baseUrl.'/index.php?id='.$prod->prod_id.'&name='.$prod->prod_name; ?>">
						<?php
							echo Yii::app()->easyImage->thumbOf(Yii::app()->params['imagesThumbEasyImageExtPath'].$prod->photo->prod_solo_1,
							    array('resize' => array('width' => 350, 'height' => 250),'sharpen' => 50, 'background' => '#ffffff','type' => 'jpg', 'quality' => 60,
							    ));
						?>
					</a>

					<div class="panel">
						<a href="<?php echo Yii::app()->baseUrl.'/index.php?id='.$prod->prod_id.'&name='.$prod->prod_name; ?>">
							<span class="metaProdName"><?php echo $prod->prod_name; ?></span>
						</a>
						<h6 class="subheader"><?php echo $prod->displayPrice(); ?></h6>
					</div>
				</div>
				<?php if ($key == 3) echo '<div class="clear"></div>'; ?>
			<?php } ?>

			<div class="clear"></div>
    	</div>

	<div class="clear"></div>

	<h3>Our Guarantee of Quality</h3>

	<p>The quality of your model replica is assured from day one of manufacture.  Our reputable model airplanes are ideally suited as collectibles, executive gifts, 
	premium and promotional incentives, art gallery sculptures, and many other purposes.  Each model is hand carved and completely 
	handpainted to match the aircrafts livery, paint scheme and registration number.  Any logo can be carved or painted on the stand 
	and a brass plaque with your personal inscription adds that extra finishing touch to your model. Please visit our Testimonial Page to 
	see the kind words that our customers have had to say about our models. </p>

	<p>All the models you see on the ModelBuffs website  are handcrafted and hand painted by our gifted artists and master
	craftsmen, and are packed with authentic detail based on the actual aircraft blueprints.   The photographs you see have not
	been manipulated or edited - they are exactly as the model looks in real life but unfortunately even digital photography cannot do full
	justice to the look and feel of the model!</p>

	<h3>How do we make our models?</h3>

	<p><strong>Most models are made to order and
	take about 10-12 weeks*</strong> to manufacture (even customised or personalised models)
	which is significantly quicker than the vast majority of our competitors. You will also notice that we offer models at
	prices way below other companies advertising on the web. We are able do this because of the direct manufacturing and selling basis
	of our business - models direct from the factory located near Clark Air Base in the Philippines, <strong>shipped through
	FedEx International Economy Courier service</strong> and delivered to your home across the Pacific or the Atlantic 3-4 days from dispatch. The price of 
	each model is <strong>inclusive of the stand of your choice and full export packing</strong>. All prices are in US Dollars. * from date of order confirmation.</p>

	<h3>Can I request made to order models?</h3>

	<p>If you do not find your required model in our database or if you have any requirements that are not covered in the
	"Made to Order" section of our site then please email us directly at ModelBuffs. We can make a
	model of any airplane or helicopter that you require – in any scale and in any size or configuration and with any markings
	or livery.</p>

	<p>Working from your photographs and our library of aircraft blueprints, our master craftsmen will meticulously
	recreate your airplane into an amazingly detailed desktop replica. Each model is hand carved from solid mahogany and completely hand
	painted to match your aircraft paint scheme and registration number. No detail will be spared!</p>

	<h5>ModelBuffs is a trade name of Wharfedale Wood Model Manufacturer and Exporter - a company duly registered in the
	Philippines.</h5>
	




