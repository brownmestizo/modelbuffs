<?php

class CategoryController extends Controller
{
	public function actionIndex()
	{

		if ($_GET['id']) 
			$this->render('index');
		else
			throw new CHttpException(404,'You are in the wrong page.');
	}

	public function actionCat() {


		if ($_GET['id']) {

			$category = TblMenus::model()->findByPk($_GET['id']);

			// Setting the page limits
			$page = (isset($_GET['page']) ? $_GET['page'] : 1);

			// Setting the Criteria
			$criteria = new CDbCriteria();
			$criteria->condition = 'prod_category = :prodCategory';
			$criteria->order = 'prod_name';
			$criteria->limit = Yii::app()->params['listPerPage'];
			$criteria->offset = $page-1;
			$criteria->params = array(':prodCategory'=>$category->menu_alias);

			$products = TblProdInfo::model()->findAll($criteria);
		    $count = TblProdInfo::model()->count($criteria);

		    // Setting the pagination
		    $pages = new CPagination($count);	    
		    $pages->setPageSize(Yii::app()->params['listPerPage']);
		    $pages->applyLimit($criteria);


			$this->render('index', array(
	            'pageSize'=>Yii::app()->params['listPerPage'],			
				'products' => $products, 			
				'category' => $category, 
				'count' => $count,
				'pages' => $pages,
			));

		} else

			throw new CHttpException(404,'The specified category cannot be found.');
	}

	

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}