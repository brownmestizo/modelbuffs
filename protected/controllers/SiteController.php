<?php

class SiteController extends Controller {

	public $layout='//layouts/main';

	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),

			'aclist'=>array(
				'class'=>'application.extensions.EAutoCompleteAction',
				'model'=>'TblProdInfo', //My model's class name
				'attribute'=>'prod_name', //The attribute of the model i will search
      		),

		);
	}

	public function accessRules() {
        return array(
            array('allow',
                'actions' => array('ajax'),
                'users' => array('*'),
            ),
        );
    }


	public function actionIndex() {

		if ($_GET['id']) {

			$product = TblProdInfo::model()->findByPk($_GET['id']);
			$stands = TblStands::model()->findAll();
			$countries = TblShippingCountries::model()->findAll();

			$this->layout='//layouts/mainNoSidebar';

			$this->render('product', array(
				'prod' => $product,
				'stands' => $stands,
				'countries' => $countries,
			));

		} else {

			$criteria = new CDbCriteria();
			$criteria->condition = 'prod_front = :prodFront';
			$criteria->order = 'prod_id';
			$criteria->params = array(':prodFront'=>'yes');

			$featuredItems = TblProdInfo::model()->findAll($criteria);
		    $count = TblProdInfo::model()->count($criteria);

			$this->render('index', array(
				'featuredItems' => $featuredItems,
				'count' => $count,
			));
		}
	}

	public function computeFinalShippingPrice($net, $currency) {
		$fuelsurcharge = Yii::app()->params['fuelsurcharge'];
		$markup = Yii::app()->params['markup'];

		$withFuelSurcharge = $net*$fuelsurcharge;
		return ($withFuelSurcharge*$currency) + $markup;
	}



	public function actionAddtlShipping () {

		$qty = $_REQUEST['qty'];
		$shippingMethod = $_REQUEST['shippingMethod'];
		$zone = $_REQUEST['zone'];
		$countryID = $_REQUEST['country'];

		$weightIDE = $_REQUEST['weightIDE'];
		$weightIDP = $_REQUEST['weightIDP'];

		$currencyE = Yii::app()->params['currencyE'];
		$currencyP = Yii::app()->params['currencyP'];

		if($shippingMethod=='economy') {
			$weight = TblShippingEconomy::model()->findByPk($weightIDE);
			$weightFinal = $qty * $weight->weight_name;
		} elseif ($shippingMethod=='priority') {
			$weight = TblShippingPriority::model()->findByPk($weightIDP);
			$weightFinal = $qty * $weight->weight_name;
		} else {
			$airparcelPrice = TblShippingCountries::model()->findByPk($countryID);
			$firstkg = $airparcelPrice->airparcel_firstkg*1.07;
			$addtlkg = ($qty-1)*($airparcelPrice->airparcel_secondkg*1.07);
			$data = number_format(($firstkg + $addtlkg)/44.5, 2, '.', '');
		}

		if($shippingMethod=='economy' or $shippingMethod=='priority') {
			$criteria = new CDbCriteria();
			$criteria->condition = 'weight_name = :weight_name';
			$criteria->params = array(':weight_name'=>$weightFinal);

			if($shippingMethod=='economy') {
				$shipping = TblShippingEconomy::model()->find($criteria);
				if ($zone) $data = $this->computeFinalShippingPrice($shipping[$zone], $currencyE); else $data = "";
			} elseif ($shippingMethod=='priority') {
				$shipping = TblShippingPriority::model()->find($criteria);
				if ($zone) $data = $this->computeFinalShippingPrice($shipping[$zone], $currencyP); else $data = "";
			}
		}

		$this->renderPartial('_shipping', array('data'=>number_format($data, 2, ".", "")));
	}





	public function actionDialog(){
		Yii::app()->clientScript->scriptMap['jquery.js'] = false;

   		$this->renderPartial('dialogs/_'.$_POST['action'], $params, false, true);
	}



	public function actionCheckout() {

		if ($_GET['id']) {

			$product = TblProdInfo::model()->findByPk($_GET['id']);
			$countries = TblShippingCountries::model()->findAll();

			$this->render('checkout', array(
				'prod' => $product,
				'countries' => $countries,
			));

		}
	}




	public function actionError() {
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


	public function actionContact() {
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}


	public function actionLogin() {
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}


	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}