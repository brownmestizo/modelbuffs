<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Modelbuffs Philippines',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	    'ext.easyimage.EasyImage'
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'modelbuffs',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				// categories
				'category/<id:\d+>/<name:\w+>'=>'category/cat',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				
				// pages				
				'/checkout'=>'site/checkout',

				// gii
  				'gii'=>'gii',
            	'gii/<controller:\w+>'=>'gii/<controller>',
            	'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',				
			),
		),

		'easyImage' => array(
		        'class' => 'application.extensions.easyimage.EasyImage',
		        //'driver' => 'GD',
		        //'quality' => 100,
		        //'cachePath' => '/assets/easyimage/',
		        //'cacheTime' => 2592000,
		        //'retinaSupport' => false,
		),

		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=rathmell_mpm',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail' => 'brownmestizo@gmail.com',	
		'listPerPage' => 20,
		'imagesThumbnailsPathCheck' => '../modelbuffs.com/mpm/uploads_thumb/',
		'imagesThumbnailsPath' => Yii::app()->request->hostInfo.'/modelbuffs.com/mpm/uploads_thumb/',
		'imagesSoloPathCheck' => '../modelbuffs.com/mpm/uploads/',
		'imagesSoloPath' => Yii::app()->request->hostInfo.'/modelbuffs.com/mpm/uploads/',		
		'imagesThumbEasyImageExt' => 'resources/images/placeholder-thumbnail.jpg',
		'imagesThumbEasyImageExtPath' => '../modelbuffs.com/mpm/uploads/',
		'website' => 'mb',
		'fuelsurcharge' => '1.18',
		'markup' => '4.5',
		'currencyE' => '1.05',
		'currencyP' => '1.07',
	),
);