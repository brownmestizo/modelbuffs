<?php

/**
 * This is the model class for table "tbl_general".
 *
 * The followings are the available columns in table 'tbl_general':
 * @property integer $prod_general
 * @property string $prod_name
 * @property string $prod_description
 * @property string $prod_description_pa
 * @property string $prod_description_m3
 * @property string $prod_keywords
 * @property string $prod_keywords_pa
 * @property string $prod_keywords_m3
 * @property string $prod_writeupkeywords
 * @property string $prod_writeupkeywords_pa
 * @property string $prod_writeupkeywords_m3
 */
class TblGeneral extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_general';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prod_name, prod_description, prod_description_pa, prod_description_m3', 'required'),
			array('prod_keywords, prod_keywords_pa, prod_keywords_m3, prod_writeupkeywords, prod_writeupkeywords_pa, prod_writeupkeywords_m3', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_general, prod_name, prod_description, prod_description_pa, prod_description_m3, prod_keywords, prod_keywords_pa, prod_keywords_m3, prod_writeupkeywords, prod_writeupkeywords_pa, prod_writeupkeywords_m3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_general' => 'Prod General',
			'prod_name' => 'Prod Name',
			'prod_description' => 'Prod Description',
			'prod_description_pa' => 'Prod Description Pa',
			'prod_description_m3' => 'Prod Description M3',
			'prod_keywords' => 'Prod Keywords',
			'prod_keywords_pa' => 'Prod Keywords Pa',
			'prod_keywords_m3' => 'Prod Keywords M3',
			'prod_writeupkeywords' => 'Prod Writeupkeywords',
			'prod_writeupkeywords_pa' => 'Prod Writeupkeywords Pa',
			'prod_writeupkeywords_m3' => 'Prod Writeupkeywords M3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prod_general',$this->prod_general);
		$criteria->compare('prod_name',$this->prod_name,true);
		$criteria->compare('prod_description',$this->prod_description,true);
		$criteria->compare('prod_description_pa',$this->prod_description_pa,true);
		$criteria->compare('prod_description_m3',$this->prod_description_m3,true);
		$criteria->compare('prod_keywords',$this->prod_keywords,true);
		$criteria->compare('prod_keywords_pa',$this->prod_keywords_pa,true);
		$criteria->compare('prod_keywords_m3',$this->prod_keywords_m3,true);
		$criteria->compare('prod_writeupkeywords',$this->prod_writeupkeywords,true);
		$criteria->compare('prod_writeupkeywords_pa',$this->prod_writeupkeywords_pa,true);
		$criteria->compare('prod_writeupkeywords_m3',$this->prod_writeupkeywords_m3,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblGeneral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function displayDescription ($alternateNames) 
	{
		switch (Yii::app()->params['website']) 
		{
			case 'mb':
				$var = $this->prod_description;
			break;

			case 'pa':
				$var = $this->prod_description_pa;
			break;

			case 'm3':
				$var = $this->prod_description_m3;
			break;
		}


		$output = mb_convert_encoding($var, "utf-8", "HTML-ENTITIES");	
		$output = preg_replace('/<p[^>]*>[\s|&nbsp;]*<\/p>/', "", $output);
		$output = preg_replace('/<br[^>]*>/', "", $output);

		$output = preg_replace('/\+\./', "", $output);
		$output = preg_replace('/\+/', "", $output);

		$models = array("[model]", "[model1]", "[model2]", "[model3]", "[model4]");
		$output = str_replace($models, $alternateNames, $output);

		return $output;
	}
}
