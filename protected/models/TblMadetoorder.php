<?php

/**
 * This is the model class for table "tbl_madetoorder".
 *
 * The followings are the available columns in table 'tbl_madetoorder':
 * @property integer $mad_id
 * @property string $mad_name
 * @property string $mad_email
 * @property string $mad_color
 * @property string $mad_length
 * @property string $mad_mark
 * @property string $mad_rem
 * @property string $mad_url
 * @property string $mad_photo
 * @property string $mad_photo2
 * @property string $mad_photo3
 */
class TblMadetoorder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_madetoorder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mad_name, mad_email, mad_color, mad_length, mad_mark, mad_rem, mad_url, mad_photo, mad_photo2, mad_photo3', 'required'),
			array('mad_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mad_id, mad_name, mad_email, mad_color, mad_length, mad_mark, mad_rem, mad_url, mad_photo, mad_photo2, mad_photo3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mad_id' => 'Mad',
			'mad_name' => 'Mad Name',
			'mad_email' => 'Mad Email',
			'mad_color' => 'Mad Color',
			'mad_length' => 'Mad Length',
			'mad_mark' => 'Mad Mark',
			'mad_rem' => 'Mad Rem',
			'mad_url' => 'Mad Url',
			'mad_photo' => 'Mad Photo',
			'mad_photo2' => 'Mad Photo2',
			'mad_photo3' => 'Mad Photo3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mad_id',$this->mad_id);
		$criteria->compare('mad_name',$this->mad_name,true);
		$criteria->compare('mad_email',$this->mad_email,true);
		$criteria->compare('mad_color',$this->mad_color,true);
		$criteria->compare('mad_length',$this->mad_length,true);
		$criteria->compare('mad_mark',$this->mad_mark,true);
		$criteria->compare('mad_rem',$this->mad_rem,true);
		$criteria->compare('mad_url',$this->mad_url,true);
		$criteria->compare('mad_photo',$this->mad_photo,true);
		$criteria->compare('mad_photo2',$this->mad_photo2,true);
		$criteria->compare('mad_photo3',$this->mad_photo3,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblMadetoorder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
