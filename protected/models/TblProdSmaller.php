<?php

/**
 * This is the model class for table "tbl_prod_smaller".
 *
 * The followings are the available columns in table 'tbl_prod_smaller':
 * @property integer $prod_id
 * @property integer $sm_enable_id
 * @property string $sm_prod_length
 * @property string $sm_prod_wingspan
 * @property string $sm_prod_height
 * @property string $sm_prod_scale
 * @property integer $sm_prod_price_id
 * @property double $sm_prod_normalprice
 * @property integer $sm_prod_shipping_price_id
 */
class TblProdSmaller extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_prod_smaller';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prod_id, sm_enable_id, sm_prod_length, sm_prod_wingspan, sm_prod_height, sm_prod_scale, sm_prod_price_id, sm_prod_normalprice, sm_prod_shipping_price_id', 'required'),
			array('prod_id, sm_enable_id, sm_prod_price_id, sm_prod_shipping_price_id', 'numerical', 'integerOnly'=>true),
			array('sm_prod_normalprice', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_id, sm_enable_id, sm_prod_length, sm_prod_wingspan, sm_prod_height, sm_prod_scale, sm_prod_price_id, sm_prod_normalprice, sm_prod_shipping_price_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_id' => 'Prod',
			'sm_enable_id' => 'Sm Enable',
			'sm_prod_length' => 'Sm Prod Length',
			'sm_prod_wingspan' => 'Sm Prod Wingspan',
			'sm_prod_height' => 'Sm Prod Height',
			'sm_prod_scale' => 'Sm Prod Scale',
			'sm_prod_price_id' => 'Sm Prod Price',
			'sm_prod_normalprice' => 'Sm Prod Normalprice',
			'sm_prod_shipping_price_id' => 'Sm Prod Shipping Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prod_id',$this->prod_id);
		$criteria->compare('sm_enable_id',$this->sm_enable_id);
		$criteria->compare('sm_prod_length',$this->sm_prod_length,true);
		$criteria->compare('sm_prod_wingspan',$this->sm_prod_wingspan,true);
		$criteria->compare('sm_prod_height',$this->sm_prod_height,true);
		$criteria->compare('sm_prod_scale',$this->sm_prod_scale,true);
		$criteria->compare('sm_prod_price_id',$this->sm_prod_price_id);
		$criteria->compare('sm_prod_normalprice',$this->sm_prod_normalprice);
		$criteria->compare('sm_prod_shipping_price_id',$this->sm_prod_shipping_price_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblProdSmaller the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
