<?php

/**
 * This is the model class for table "tbl_shipping_countries".
 *
 * The followings are the available columns in table 'tbl_shipping_countries':
 * @property integer $cty_id
 * @property string $cty_name
 * @property string $cty_priority
 * @property string $cty_economy
 * @property integer $zone_id
 * @property string $airparcel_firstkg
 * @property string $airparcel_secondkg
 */
class TblShippingCountries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_shipping_countries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cty_name, cty_priority, cty_economy, zone_id', 'required'),
			array('zone_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cty_id, cty_name, cty_priority, cty_economy, zone_id, airparcel_firstkg, airparcel_secondkg', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'shipping_zone' => array(self::BELONGS_TO, 'TblShippingZones', 'zone_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cty_id' => 'Cty',
			'cty_name' => 'Cty Name',
			'cty_priority' => 'Cty Priority',
			'cty_economy' => 'Cty Economy',
			'zone_id' => 'Zone',
			'airparcel_firstkg' => 'Air Parcel First KG',
			'airparcel_secondkg' => 'Air Parcel Second KG',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cty_id',$this->cty_id);
		$criteria->compare('cty_name',$this->cty_name,true);
		$criteria->compare('cty_priority',$this->cty_priority,true);
		$criteria->compare('cty_economy',$this->cty_economy,true);
		$criteria->compare('zone_id',$this->zone_id);
		$criteria->compare('airparcel_firstkg',$this->airparcel_firstkg,true);
		$criteria->compare('airparcel_secondkg',$this->airparcel_secondkg,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblShippingCountries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function computeEconomyPrice ($weight)
    {
    	if ($this->cty_economy=='Yes') {
			$criteria = new CDbCriteria;
			$criteria->select = $this->shipping_zone->zone_name;
			$criteria->condition = "weight_ide = :weight_ide";
			$criteria->params=array(':weight_ide' => $weight);

			$results = TblShippingEconomy::model()->find($criteria);

			$shippingPrice = $results[$this->shipping_zone->zone_name];
			$withFuelSurcharge = $shippingPrice*1.18;
			$data = ($withFuelSurcharge*1.05) + 4.5;

			return $data;

		} else return NULL;
    }

    public function computePriorityPrice ($weight)
    {
    	if ($this->cty_priority=='Yes') {
			$criteria = new CDbCriteria;
			$criteria->select = $this->shipping_zone->zone_name;
			$criteria->condition = "weight_idp = :weight_idp";
			$criteria->params=array(':weight_idp' => $weight);

			$results = TblShippingPriority::model()->find($criteria);

			$shippingPrice = $results[$this->shipping_zone->zone_name];
			$withFuelSurcharge = $shippingPrice*1.18;
			$data = ($withFuelSurcharge*1.07) + 4.5;

			return $data;
		} else return NULL;
    }

}
