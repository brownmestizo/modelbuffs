<?php

/**
 * This is the model class for table "tbl_prod_info".
 *
 * The followings are the available columns in table 'tbl_prod_info':
 * @property integer $prod_id
 * @property integer $prod_price_id
 * @property string $prod_name
 * @property string $prod_alt1
 * @property string $prod_alt2
 * @property string $prod_alt3
 * @property string $prod_alt4
 * @property string $prod_code
 * @property string $prod_category
 * @property integer $prod_category_shipping
 * @property string $prod_writeup
 * @property string $prod_length
 * @property string $prod_wingspan
 * @property string $prod_height
 * @property string $prod_scale
 * @property string $prod_links
 * @property string $prod_linkdescription
 * @property string $prod_front
 * @property string $prod_keywords
 * @property string $prod_keywords_writeup
 * @property string $prod_title
 * @property string $prod_description
 * @property integer $prod_general
 * @property integer $prod_era
 * @property string $prod_company
 * @property string $prod_related
 * @property string $prod_related_pa
 * @property string $prod_related_m3
 * @property string $prod_related2
 * @property integer $prod_saveas
 * @property string $prod_aircraftreg
 * @property integer $mb
 * @property integer $pa
 * @property integer $m3
 */
class TblProdInfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_prod_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prod_price_id, prod_name, prod_alt1, prod_alt2, prod_alt3, prod_alt4, prod_code, prod_category, prod_writeup, prod_length, prod_wingspan, prod_height, prod_scale, prod_links, prod_linkdescription, prod_keywords, prod_keywords_writeup, prod_title, prod_description, prod_general, prod_era, prod_company, prod_related, prod_related2, prod_saveas, prod_aircraftreg', 'required'),
			array('prod_price_id, prod_category_shipping, prod_general, prod_era, prod_saveas, mb, pa, m3', 'numerical', 'integerOnly'=>true),
			array('prod_front, prod_related_pa, prod_related_m3', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_id, prod_price_id, prod_name, prod_alt1, prod_alt2, prod_alt3, prod_alt4, prod_code, prod_category, prod_category_shipping, prod_writeup, prod_length, prod_wingspan, prod_height, prod_scale, prod_links, prod_linkdescription, prod_front, prod_keywords, prod_keywords_writeup, prod_title, prod_description, prod_general, prod_era, prod_company, prod_related, prod_related_pa, prod_related_m3, prod_related2, prod_saveas, prod_aircraftreg, mb, pa, m3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'photo' => array(self::HAS_ONE, 'TblProdPhotos', 'prod_id'),
			'price' => array(self::HAS_ONE, 'TblProdPrices', 'prod_id'),
			'pricingCategory' => array(self::HAS_ONE, 'TblProdPricing', array('prod_price_id'=>'prod_price_id')),
			'smallerModelPrice' => array(self::HAS_ONE, 'TblProdSmaller', array('prod_id'=>'prod_id')),
			'generalDescription' => array(self::HAS_ONE, 'TblGeneral', array('prod_general'=>'prod_general')),
			'shippingCategory' => array(self::HAS_ONE, 'TblShippingCategories', array('prod_shipping_price_id'=>'prod_category_shipping')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_id' => 'Product ID',
			'prod_price_id' => 'Pricing Category',
			'prod_name' => 'Full Product Name',
			'prod_alt1' => 'Alternate Name',
			'prod_alt2' => 'Alternate Name',
			'prod_alt3' => 'Alternate Name',
			'prod_alt4' => 'Alternate Name',
			'prod_code' => 'Product Code',
			'prod_category' => 'Category',
			'prod_category_shipping' => 'Shipping Category',
			'prod_writeup' => 'Model Specific Description', //difference of writeup and description?
			'prod_length' => 'Length',
			'prod_wingspan' => 'Wing Span',
			'prod_height' => 'Height',
			'prod_scale' => 'Scale',
			'prod_links' => 'Related Link',
			'prod_linkdescription' => 'Related Link Description',
			'prod_front' => 'Featured Product',
			'prod_keywords' => 'Keywords',
			'prod_keywords_writeup' => 'Meta Description',
			'prod_title' => 'Prod Title',
			'prod_description' => 'Description',
			'prod_general' => 'Prod General',
			'prod_era' => 'Prod Era',
			'prod_company' => 'Prod Company',
			'prod_related' => 'Prod Related',
			'prod_related_pa' => 'Prod Related Pa',
			'prod_related_m3' => 'Prod Related M3',
			'prod_related2' => 'Prod Related2',
			'prod_saveas' => 'Prod Saveas',
			'prod_aircraftreg' => 'Aircraft Registration',
			'mb' => 'Mb',
			'pa' => 'Pa',
			'm3' => 'M3',
			'displayPrice' => 'Regular Price',
			'displaySmallerPrice' => 'Smaller Model Price',
			'generalDescription' => 'General Model Information',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prod_id',$this->prod_id);
		$criteria->compare('prod_price_id',$this->prod_price_id);
		$criteria->compare('prod_name',$this->prod_name,true);
		$criteria->compare('prod_alt1',$this->prod_alt1,true);
		$criteria->compare('prod_alt2',$this->prod_alt2,true);
		$criteria->compare('prod_alt3',$this->prod_alt3,true);
		$criteria->compare('prod_alt4',$this->prod_alt4,true);
		$criteria->compare('prod_code',$this->prod_code,true);
		$criteria->compare('prod_category',$this->prod_category,true);
		$criteria->compare('prod_category_shipping',$this->prod_category_shipping);
		$criteria->compare('prod_writeup',$this->prod_writeup,true);
		$criteria->compare('prod_length',$this->prod_length,true);
		$criteria->compare('prod_wingspan',$this->prod_wingspan,true);
		$criteria->compare('prod_height',$this->prod_height,true);
		$criteria->compare('prod_scale',$this->prod_scale,true);
		$criteria->compare('prod_links',$this->prod_links,true);
		$criteria->compare('prod_linkdescription',$this->prod_linkdescription,true);
		$criteria->compare('prod_front',$this->prod_front,true);
		$criteria->compare('prod_keywords',$this->prod_keywords,true);
		$criteria->compare('prod_keywords_writeup',$this->prod_keywords_writeup,true);
		$criteria->compare('prod_title',$this->prod_title,true);
		$criteria->compare('prod_description',$this->prod_description,true);
		$criteria->compare('prod_general',$this->prod_general);
		$criteria->compare('prod_era',$this->prod_era);
		$criteria->compare('prod_company',$this->prod_company,true);
		$criteria->compare('prod_related',$this->prod_related,true);
		$criteria->compare('prod_related_pa',$this->prod_related_pa,true);
		$criteria->compare('prod_related_m3',$this->prod_related_m3,true);
		$criteria->compare('prod_related2',$this->prod_related2,true);
		$criteria->compare('prod_saveas',$this->prod_saveas);
		$criteria->compare('prod_aircraftreg',$this->prod_aircraftreg,true);
		$criteria->compare('mb',$this->mb);
		$criteria->compare('pa',$this->pa);
		$criteria->compare('m3',$this->m3);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblProdInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public function defaultScope()
    {
        return array(
            'order' => "prod_id DESC",
            'condition' => Yii::app()->params['website']."= 1",
        );
    }

    public function getPricesFromCat ($pricingCategoryId)
    {
		return TblProdPricing::model()->findByPk($pricingCategoryId);
    }

    public function getAlternateNames ()
    {
    	return array($this->prod_name, $this->prod_alt1, $this->prod_alt2, $this->prod_alt3, $this->prod_alt4);
    }


    public function displayPrice ($currency)
    {

    	if ($this->prod_price_id>0)
    		return $currency.$this->pricingCategory->prod_price_price;
    }

    public function displaySmallerPrice ($currency) {

    	if ($this->smallerModelPrice->prod_id) {
    		if ($this->smallerModelPrice->sm_prod_price_id)
			{
				$price = $this->getPricesFromCat($this->smallerModelPrice->sm_prod_price_id);
				return $currency.$price->prod_price_price;
			}
			else
			{
				return $currency.$this->smallerModelPrice->sm_prod_normalprice;
			}
    	}

    }


    public function displayThumbnail ()
    {
		$image = Yii::app()->params['imagesThumbnailsPath'].$this->photo->prod_solo_1;

		if (!file_exists(Yii::app()->params['imagesThumbnailsPathCheck'].$this->photo->prod_solo_1)) {
			$image = Yii::app()->request->baseUrl.'/resources/images/placeholder-thumbnail.jpg';
		}

		return $image;
    }

    public function displayInchesToCm ($attribute, $unit)
    {
    	if ($attribute) {
	    	switch ($unit) {
	    		case 'in' :
	    			$newAttribute = $attribute * 2.54;
	    		break;
	    	}

			$fullAttribute = number_format($attribute, 2)." ".$unit;
			$fullAttribute .= " <em>(".number_format($newAttribute, 2)." cm)</em>";

	    	return $fullAttribute;
	    }
    }

    public function displayContent ($header, $content)
    {
    	if ($content)
    	{
    		$header =  '<h3>'.$this->getAttributeLabel($header).'</h3>';
    		$content = $this->displayHTML($content);
    		return $header.$content;
    	}
    }


    public function displayHTML ($content)
    {
    	$output = html_entity_decode($content);
    	$output = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $output);
    	return $output;
    }


    public function displayAttribute ($header, $attribute)
    {
    	if ($attribute)
    	{
    		$output = '<span class="metaContainer">';
    		$output .= '<span class="metaAttribute">'.$this->getAttributeLabel($header).'</span> ';
    		$output .= $attribute.'</span>';
    		return $output;
    	}
    }

}
