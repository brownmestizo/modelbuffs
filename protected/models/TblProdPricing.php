<?php

/**
 * This is the model class for table "tbl_prod_pricing".
 *
 * The followings are the available columns in table 'tbl_prod_pricing':
 * @property integer $prod_price_id
 * @property string $prod_price_name
 * @property string $prod_price_description
 * @property string $prod_price_price
 */
class TblProdPricing extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_prod_pricing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prod_price_name, prod_price_description, prod_price_price', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_price_id, prod_price_name, prod_price_description, prod_price_price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_price_id' => 'Prod Price',
			'prod_price_name' => 'Prod Price Name',
			'prod_price_description' => 'Prod Price Description',
			'prod_price_price' => 'Prod Price Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prod_price_id',$this->prod_price_id);
		$criteria->compare('prod_price_name',$this->prod_price_name,true);
		$criteria->compare('prod_price_description',$this->prod_price_description,true);
		$criteria->compare('prod_price_price',$this->prod_price_price,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblProdPricing the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
