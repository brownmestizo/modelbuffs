<?php

/**
 * This is the model class for table "tbl_prod_photos".
 *
 * The followings are the available columns in table 'tbl_prod_photos':
 * @property integer $prod_id
 * @property string $prod_solo_1
 * @property string $prod_solo_2
 * @property string $prod_solo_3
 * @property string $prod_solo_4
 * @property string $prod_solo_1_pa
 * @property string $prod_solo_2_pa
 * @property string $prod_solo_3_pa
 * @property string $prod_solo_4_pa
 * @property string $prod_solo_1_m3
 * @property string $prod_solo_2_m3
 * @property string $prod_solo_3_m3
 * @property string $prod_solo_4_m3
 * @property string $prod_solo_1_blank
 * @property string $prod_solo_2_blank
 * @property string $prod_solo_3_blank
 * @property string $prod_solo_4_blank
 */
class TblProdPhotos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_prod_photos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prod_solo_1, prod_solo_2, prod_solo_3, prod_solo_4', 'required'),
			array('prod_solo_1_pa, prod_solo_2_pa, prod_solo_3_pa, prod_solo_4_pa, prod_solo_1_m3, prod_solo_2_m3, prod_solo_3_m3, prod_solo_4_m3, prod_solo_1_blank, prod_solo_2_blank, prod_solo_3_blank, prod_solo_4_blank', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_id, prod_solo_1, prod_solo_2, prod_solo_3, prod_solo_4, prod_solo_1_pa, prod_solo_2_pa, prod_solo_3_pa, prod_solo_4_pa, prod_solo_1_m3, prod_solo_2_m3, prod_solo_3_m3, prod_solo_4_m3, prod_solo_1_blank, prod_solo_2_blank, prod_solo_3_blank, prod_solo_4_blank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_id' => 'Prod',
			'prod_solo_1' => 'Prod Solo 1',
			'prod_solo_2' => 'Prod Solo 2',
			'prod_solo_3' => 'Prod Solo 3',
			'prod_solo_4' => 'Prod Solo 4',
			'prod_solo_1_pa' => 'Prod Solo 1 Pa',
			'prod_solo_2_pa' => 'Prod Solo 2 Pa',
			'prod_solo_3_pa' => 'Prod Solo 3 Pa',
			'prod_solo_4_pa' => 'Prod Solo 4 Pa',
			'prod_solo_1_m3' => 'Prod Solo 1 M3',
			'prod_solo_2_m3' => 'Prod Solo 2 M3',
			'prod_solo_3_m3' => 'Prod Solo 3 M3',
			'prod_solo_4_m3' => 'Prod Solo 4 M3',
			'prod_solo_1_blank' => 'Prod Solo 1 Blank',
			'prod_solo_2_blank' => 'Prod Solo 2 Blank',
			'prod_solo_3_blank' => 'Prod Solo 3 Blank',
			'prod_solo_4_blank' => 'Prod Solo 4 Blank',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prod_id',$this->prod_id);
		$criteria->compare('prod_solo_1',$this->prod_solo_1,true);
		$criteria->compare('prod_solo_2',$this->prod_solo_2,true);
		$criteria->compare('prod_solo_3',$this->prod_solo_3,true);
		$criteria->compare('prod_solo_4',$this->prod_solo_4,true);
		$criteria->compare('prod_solo_1_pa',$this->prod_solo_1_pa,true);
		$criteria->compare('prod_solo_2_pa',$this->prod_solo_2_pa,true);
		$criteria->compare('prod_solo_3_pa',$this->prod_solo_3_pa,true);
		$criteria->compare('prod_solo_4_pa',$this->prod_solo_4_pa,true);
		$criteria->compare('prod_solo_1_m3',$this->prod_solo_1_m3,true);
		$criteria->compare('prod_solo_2_m3',$this->prod_solo_2_m3,true);
		$criteria->compare('prod_solo_3_m3',$this->prod_solo_3_m3,true);
		$criteria->compare('prod_solo_4_m3',$this->prod_solo_4_m3,true);
		$criteria->compare('prod_solo_1_blank',$this->prod_solo_1_blank,true);
		$criteria->compare('prod_solo_2_blank',$this->prod_solo_2_blank,true);
		$criteria->compare('prod_solo_3_blank',$this->prod_solo_3_blank,true);
		$criteria->compare('prod_solo_4_blank',$this->prod_solo_4_blank,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblProdPhotos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
