<?php

/**
 * This is the model class for table "tbl_prod_prices".
 *
 * The followings are the available columns in table 'tbl_prod_prices':
 * @property integer $prod_id
 * @property string $prod_normalprice
 * @property string $prod_salesprice
 * @property string $prod_shipping_asia
 * @property string $prod_shipping_europe
 * @property string $prod_shipping_usa
 * @property string $prod_shipping_canada
 * @property string $prod_shipping_au
 * @property string $prod_shipping_asia_parcel
 * @property string $prod_shipping_europe_parcel
 * @property string $prod_shipping_usa_parcel
 * @property string $prod_shipping_canada_parcel
 * @property string $prod_shipping_au_parcel
 * @property string $prod_customprice
 */
class TblProdPrices extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_prod_prices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prod_normalprice, prod_salesprice, prod_shipping_asia, prod_shipping_europe, prod_shipping_usa, prod_shipping_canada, prod_shipping_au, prod_shipping_asia_parcel, prod_shipping_europe_parcel, prod_shipping_usa_parcel, prod_shipping_canada_parcel, prod_shipping_au_parcel, prod_customprice', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_id, prod_normalprice, prod_salesprice, prod_shipping_asia, prod_shipping_europe, prod_shipping_usa, prod_shipping_canada, prod_shipping_au, prod_shipping_asia_parcel, prod_shipping_europe_parcel, prod_shipping_usa_parcel, prod_shipping_canada_parcel, prod_shipping_au_parcel, prod_customprice', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_id' => 'Prod',
			'prod_normalprice' => 'Prod Normalprice',
			'prod_salesprice' => 'Prod Salesprice',
			'prod_shipping_asia' => 'Prod Shipping Asia',
			'prod_shipping_europe' => 'Prod Shipping Europe',
			'prod_shipping_usa' => 'Prod Shipping Usa',
			'prod_shipping_canada' => 'Prod Shipping Canada',
			'prod_shipping_au' => 'Prod Shipping Au',
			'prod_shipping_asia_parcel' => 'Prod Shipping Asia Parcel',
			'prod_shipping_europe_parcel' => 'Prod Shipping Europe Parcel',
			'prod_shipping_usa_parcel' => 'Prod Shipping Usa Parcel',
			'prod_shipping_canada_parcel' => 'Prod Shipping Canada Parcel',
			'prod_shipping_au_parcel' => 'Prod Shipping Au Parcel',
			'prod_customprice' => 'Prod Customprice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prod_id',$this->prod_id);
		$criteria->compare('prod_normalprice',$this->prod_normalprice,true);
		$criteria->compare('prod_salesprice',$this->prod_salesprice,true);
		$criteria->compare('prod_shipping_asia',$this->prod_shipping_asia,true);
		$criteria->compare('prod_shipping_europe',$this->prod_shipping_europe,true);
		$criteria->compare('prod_shipping_usa',$this->prod_shipping_usa,true);
		$criteria->compare('prod_shipping_canada',$this->prod_shipping_canada,true);
		$criteria->compare('prod_shipping_au',$this->prod_shipping_au,true);
		$criteria->compare('prod_shipping_asia_parcel',$this->prod_shipping_asia_parcel,true);
		$criteria->compare('prod_shipping_europe_parcel',$this->prod_shipping_europe_parcel,true);
		$criteria->compare('prod_shipping_usa_parcel',$this->prod_shipping_usa_parcel,true);
		$criteria->compare('prod_shipping_canada_parcel',$this->prod_shipping_canada_parcel,true);
		$criteria->compare('prod_shipping_au_parcel',$this->prod_shipping_au_parcel,true);
		$criteria->compare('prod_customprice',$this->prod_customprice,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblProdPrices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
