<?php

/**
 * This is the model class for table "tbl_shipping_economy".
 *
 * The followings are the available columns in table 'tbl_shipping_economy':
 * @property integer $weight_ide
 * @property string $weight_name
 * @property string $A
 * @property string $B
 * @property string $C
 * @property string $D
 * @property string $E
 * @property string $F
 * @property string $G
 * @property string $H
 * @property string $K
 * @property string $N
 * @property string $O
 * @property string $P
 * @property string $Q
 * @property string $R
 * @property string $T
 * @property string $U
 * @property string $V
 * @property string $W
 * @property string $X
 * @property string $Y
 * @property string $Z
 * @property string $AA
 * @property string $AB
 */
class TblShippingEconomy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_shipping_economy';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('weight_name, A, B, C, D, E, F, G, H, K, N, O, P, Q, R, T, U, V, W, X, Y, Z, AA, AB', 'required'),
			array('A, B, C, D, E, F, G, H, K, N, O, P, Q, R, T, U, V, W, X, Y, Z, AA, AB', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('weight_ide, weight_name, A, B, C, D, E, F, G, H, K, N, O, P, Q, R, T, U, V, W, X, Y, Z, AA, AB', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'weight_ide' => 'Weight Ide',
			'weight_name' => 'Weight Name',
			'A' => 'A',
			'B' => 'B',
			'C' => 'C',
			'D' => 'D',
			'E' => 'E',
			'F' => 'F',
			'G' => 'G',
			'H' => 'H',
			'K' => 'K',
			'N' => 'N',
			'O' => 'O',
			'P' => 'P',
			'Q' => 'Q',
			'R' => 'R',
			'T' => 'T',
			'U' => 'U',
			'V' => 'V',
			'W' => 'W',
			'X' => 'X',
			'Y' => 'Y',
			'Z' => 'Z',
			'AA' => 'Aa',
			'AB' => 'Ab',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('weight_ide',$this->weight_ide);
		$criteria->compare('weight_name',$this->weight_name,true);
		$criteria->compare('A',$this->A,true);
		$criteria->compare('B',$this->B,true);
		$criteria->compare('C',$this->C,true);
		$criteria->compare('D',$this->D,true);
		$criteria->compare('E',$this->E,true);
		$criteria->compare('F',$this->F,true);
		$criteria->compare('G',$this->G,true);
		$criteria->compare('H',$this->H,true);
		$criteria->compare('K',$this->K,true);
		$criteria->compare('N',$this->N,true);
		$criteria->compare('O',$this->O,true);
		$criteria->compare('P',$this->P,true);
		$criteria->compare('Q',$this->Q,true);
		$criteria->compare('R',$this->R,true);
		$criteria->compare('T',$this->T,true);
		$criteria->compare('U',$this->U,true);
		$criteria->compare('V',$this->V,true);
		$criteria->compare('W',$this->W,true);
		$criteria->compare('X',$this->X,true);
		$criteria->compare('Y',$this->Y,true);
		$criteria->compare('Z',$this->Z,true);
		$criteria->compare('AA',$this->AA,true);
		$criteria->compare('AB',$this->AB,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblShippingEconomy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
