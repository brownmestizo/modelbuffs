<?php

/**
 * This is the model class for table "tbl_shipping_categories".
 *
 * The followings are the available columns in table 'tbl_shipping_categories':
 * @property integer $prod_shipping_price_id
 * @property string $prod_shipping_name
 * @property integer $weight_ide
 * @property integer $weight_idp
 */
class TblShippingCategories extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_shipping_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prod_shipping_name, weight_ide, weight_idp', 'required'),
			array('weight_ide, weight_idp', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_shipping_price_id, prod_shipping_name, weight_ide, weight_idp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_shipping_price_id' => 'Prod Shipping Price',
			'prod_shipping_name' => 'Prod Shipping Name',
			'weight_ide' => 'Weight Ide',
			'weight_idp' => 'Weight Idp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prod_shipping_price_id',$this->prod_shipping_price_id);
		$criteria->compare('prod_shipping_name',$this->prod_shipping_name,true);
		$criteria->compare('weight_ide',$this->weight_ide);
		$criteria->compare('weight_idp',$this->weight_idp);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblShippingCategories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
