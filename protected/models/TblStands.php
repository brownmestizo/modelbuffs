<?php

/**
 * This is the model class for table "tbl_stands".
 *
 * The followings are the available columns in table 'tbl_stands':
 * @property integer $stand_id
 * @property string $stand_name
 * @property integer $stand_price
 * @property integer $mb
 * @property integer $pa
 * @property integer $m3
 */
class TblStands extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_stands';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('stand_name', 'required'),
			array('stand_id, stand_price, mb, pa, m3', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('stand_id, stand_name, stand_price, mb, pa, m3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'stand_id' => 'Stand',
			'stand_name' => 'Stand Name',
			'stand_price' => 'Stand Price',
			'mb' => 'Mb',
			'pa' => 'Pa',
			'm3' => 'M3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('stand_id',$this->stand_id);
		$criteria->compare('stand_name',$this->stand_name,true);
		$criteria->compare('stand_price',$this->stand_price);
		$criteria->compare('mb',$this->mb);
		$criteria->compare('pa',$this->pa);
		$criteria->compare('m3',$this->m3);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblStands the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
