<?php

/**
 * This is the model class for table "tbl_menus".
 *
 * The followings are the available columns in table 'tbl_menus':
 * @property integer $menu_id
 * @property string $menu_alias
 * @property string $menu_name
 * @property string $menu_status
 * @property string $menu_title
 * @property string $menu_description
 * @property string $menu_writeup
 * @property string $menu_status_pa
 * @property string $menu_description_pa
 * @property string $menu_writeup_pa
 * @property string $menu_status_m3
 * @property string $menu_description_m3
 * @property string $menu_writeup_m3
 * @property string $menu_writeupkeywords
 * @property string $menu_writeupkeywords_pa
 * @property string $menu_writeupkeywords_m3
 */
class TblMenus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_menus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menu_alias, menu_name, menu_status, menu_title, menu_description, menu_status_pa, menu_description_pa, menu_status_m3, menu_description_m3, menu_writeupkeywords, menu_writeupkeywords_pa, menu_writeupkeywords_m3', 'required'),
			array('menu_writeup, menu_writeup_pa, menu_writeup_m3', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('menu_id, menu_alias, menu_name, menu_status, menu_title, menu_description, menu_writeup, menu_status_pa, menu_description_pa, menu_writeup_pa, menu_status_m3, menu_description_m3, menu_writeup_m3, menu_writeupkeywords, menu_writeupkeywords_pa, menu_writeupkeywords_m3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'menu_id' => 'Menu',
			'menu_alias' => 'Menu Alias',
			'menu_name' => 'Menu Name',
			'menu_status' => 'Menu Status',
			'menu_title' => 'Menu Title',
			'menu_description' => 'Menu Description',
			'menu_writeup' => 'Menu Writeup',
			'menu_status_pa' => 'Menu Status Pa',
			'menu_description_pa' => 'Menu Description Pa',
			'menu_writeup_pa' => 'Menu Writeup Pa',
			'menu_status_m3' => 'Menu Status M3',
			'menu_description_m3' => 'Menu Description M3',
			'menu_writeup_m3' => 'Menu Writeup M3',
			'menu_writeupkeywords' => 'Write Up Keywords',
			'menu_writeupkeywords_pa' => 'Write Up Keywords Pa',
			'menu_writeupkeywords_m3' => 'Write Up Keywords M3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('menu_id',$this->menu_id);
		$criteria->compare('menu_alias',$this->menu_alias,true);
		$criteria->compare('menu_name',$this->menu_name,true);
		$criteria->compare('menu_status',$this->menu_status,true);
		$criteria->compare('menu_title',$this->menu_title,true);
		$criteria->compare('menu_description',$this->menu_description,true);
		$criteria->compare('menu_writeup',$this->menu_writeup,true);
		$criteria->compare('menu_status_pa',$this->menu_status_pa,true);
		$criteria->compare('menu_description_pa',$this->menu_description_pa,true);
		$criteria->compare('menu_writeup_pa',$this->menu_writeup_pa,true);
		$criteria->compare('menu_status_m3',$this->menu_status_m3,true);
		$criteria->compare('menu_description_m3',$this->menu_description_m3,true);
		$criteria->compare('menu_writeup_m3',$this->menu_writeup_m3,true);
		$criteria->compare('menu_writeupkeywords',$this->menu_writeupkeywords,true);
		$criteria->compare('menu_writeupkeywords_pa',$this->menu_writeupkeywords_pa,true);
		$criteria->compare('menu_writeupkeywords_m3',$this->menu_writeupkeywords_m3,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblMenus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function encodedMenuWriteup() 
	{
		$output = preg_replace('/\+/', "", preg_replace('/\+\./', "", $this->encode("{$this->menu_writeup}")));
		$output = preg_replace('/<p[^>]*>[\s|&nbsp;]*<\/p>/', "", $output);
		return  $output;
	}

	public function encode($html) 
	{
		return mb_convert_encoding($html, "utf-8", "HTML-ENTITIES");
	}

}
