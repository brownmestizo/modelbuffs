<?php

class NavMenuWidget extends CWidget {

	public $menuCategory=null;

    public function run() {

    	switch ($this->menuCategory) {
			case 'navTop':
				$this->render('navTopWidget');
			break;
			case 'navBottom':
				$this->render('navBottomWidget');
			break;
			case 'categories':
				$menus = TblMenus::model()->findAll();
				$this->render('categoriesMenuWidget', array('menus'=>$menus));
			break;
    	}


    }

}

?>